namespace java.swift net.gdface.facedb.decorator
namespace cpp gdface
namespace java net.gdface.facedb.thrift.client
namespace py gdface.thrift


enum MatType {
  NV21,
  RGB,
  BGR,
  RGBA,
  GRAY
}

struct FAngle {
  1: required double confidence;
  2: required i32 pitch;
  3: required i32 roll;
  4: required i32 yaw;
}

struct EyeInfo {
  1: required i32 leftx;
  2: required i32 lefty;
  3: required i32 rightx;
  4: required i32 righty;
}

struct FInt2 {
  1: required i32 x;
  2: required i32 y;
}

struct FRect {
  1: required i32 height;
  2: required i32 left;
  3: required i32 top;
  4: required i32 width;
}

exception DuplicateRecordException {
  1: optional string message;
  2: optional string causeClass;
  3: optional string serviceStackTraceMessage;
  4: optional string causeFields;
}

exception ServiceRuntimeException {
  1: optional string message;
  2: optional string causeClass;
  3: optional string serviceStackTraceMessage;
  4: optional string causeFields;
  5: required i32 type;
}

struct FeatureBean {
  1: required bool _new;
  2: required i32 modified;
  3: required i32 initialized;
  4: optional string md5;
  5: optional binary feature;
  6: optional i64 createTime;
}

struct ImageBean {
  1: required bool _new;
  2: required i32 modified;
  3: required i32 initialized;
  4: optional string md5;
  5: optional string format;
  6: optional i32 width;
  7: optional i32 height;
  8: optional i32 depth;
  9: optional i32 faceNum;
  10: optional string location;
  11: optional i64 updateTime;
  12: optional i64 createTime;
}

exception ImageErrorException {
  1: optional string message;
  2: optional string causeClass;
  3: optional string serviceStackTraceMessage;
  4: optional string causeFields;
}

exception NotFaceDetectedException {
  1: optional string message;
  2: optional string causeClass;
  3: optional string serviceStackTraceMessage;
  4: optional string causeFields;
  5: required i32 codeNum;
  6: required i32 faceNum;
}

exception NotFoundBeanException {
  1: optional string message;
  2: optional string causeClass;
  3: optional string serviceStackTraceMessage;
  4: optional string causeFields;
}

struct SearchResult {
  1: optional binary featureId;
  2: optional string hexFeatureId;
  3: optional string owner;
  4: required double similarity;
}

struct FaceBean {
  1: required bool _new;
  2: required i32 modified;
  3: required i32 initialized;
  4: optional i32 id;
  5: optional string imageMd5;
  6: optional string featureMd5;
  7: optional i32 faceLeft;
  8: optional i32 faceTop;
  9: optional i32 faceWidth;
  10: optional i32 faceHeight;
  11: optional i32 eyeLeftx;
  12: optional i32 eyeLefty;
  13: optional i32 eyeRightx;
  14: optional i32 eyeRighty;
  15: optional i32 mouthX;
  16: optional i32 mouthY;
  17: optional i32 noseX;
  18: optional i32 noseY;
  19: optional i32 angleYaw;
  20: optional i32 anglePitch;
  21: optional i32 angleRoll;
  22: optional double angleConfidence;
  23: optional binary extInfo;
  24: optional i64 createTime;
}

struct CodeInfo {
  1: optional FAngle angle;
  2: optional binary code;
  3: optional EyeInfo ei;
  4: optional binary facialData;
  5: optional FInt2 mouth;
  6: optional FInt2 nose;
  7: optional FInt2 offset;
  8: optional FRect pos;
  9: required i32 rotate;
}

struct CompareResult {
  1: optional list<CodeInfo> codes;
  2: optional list<double> similartys;
}

service FaceDb {
  FeatureBean addFeature(1: optional binary feature, 2: optional map<binary, CodeInfo> faces) throws (1: DuplicateRecordException ex1, 2: ServiceRuntimeException ex2);
  ImageBean addImage(1: optional binary imgData, 2: optional list<CodeInfo> features) throws (1: DuplicateRecordException ex1, 2: ServiceRuntimeException ex2);
  ImageBean addImageIfAbsent(1: optional binary imgData, 2: optional CodeInfo code, 3: required double similarty) throws (1: ImageErrorException ex1, 2: NotFaceDetectedException ex2, 3: ServiceRuntimeException ex3);
  list<double> compareFaces(1: optional string featureId, 2: optional binary imgData, 3: optional list<CodeInfo> facePos) throws (1: NotFoundBeanException ex1, 2: NotFaceDetectedException ex2, 3: ServiceRuntimeException ex3);
  double compareFeature(1: optional string featureId, 2: optional binary feature) throws (1: NotFoundBeanException ex1, 2: ServiceRuntimeException ex2);
  double compareFeatureId(1: optional string featureId1, 2: optional string featureId2) throws (1: NotFoundBeanException ex1, 2: ServiceRuntimeException ex2);
  list<double> compareFeatures(1: optional string featureId, 2: optional list<CodeInfo> features) throws (1: NotFoundBeanException ex1, 2: ServiceRuntimeException ex2);
  map<string, string> dbCapacity() throws (1: ServiceRuntimeException ex1);
  bool deleteFeature(1: optional string featureId, 2: required bool cascade) throws (1: ServiceRuntimeException ex1);
  i32 deleteFeatures(1: optional list<string> featureIdList, 2: required bool cascade) throws (1: ServiceRuntimeException ex1);
  bool deleteImage(1: optional string imgMd5, 2: required bool cascade) throws (1: ServiceRuntimeException ex1);
  i32 deleteImages(1: optional list<string> imgMd5List, 2: required bool cascade) throws (1: ServiceRuntimeException ex1);
  ImageBean detectAndAddFeatures(1: optional binary imgData, 2: required i32 faceNum) throws (1: ImageErrorException ex1, 2: DuplicateRecordException ex2, 3: NotFaceDetectedException ex3, 4: ServiceRuntimeException ex4);
  CompareResult detectAndCompareFaces(1: optional string featureId, 2: optional binary imgData, 3: required i32 faceNum) throws (1: NotFoundBeanException ex1, 2: ImageErrorException ex2, 3: NotFaceDetectedException ex3, 4: ServiceRuntimeException ex4);
  list<CodeInfo> detectAndGetCodeInfo(1: optional binary imgData) throws (1: ImageErrorException ex1, 2: ServiceRuntimeException ex2);
  list<CodeInfo> detectAndGetCodeInfoMat(1: optional MatType matType, 2: optional binary matData, 3: required i32 width, 4: required i32 height) throws (1: ImageErrorException ex1, 2: ServiceRuntimeException ex2);
  list<SearchResult> detectAndSearchFaces(1: optional binary imgData, 2: required double similarty, 3: required i32 rows, 4: optional string where) throws (1: ImageErrorException ex1, 2: NotFaceDetectedException ex2, 3: ServiceRuntimeException ex3);
  CodeInfo getCodeInfo(1: required i32 faceId) throws (1: ServiceRuntimeException ex1);
  CodeInfo getCodeInfoByFeatureId(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  CodeInfo getCodeInfoByImageMd5(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  list<CodeInfo> getCodeInfosByFeatureId(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  list<CodeInfo> getCodeInfosByImageMd5(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  FaceBean getFace(1: required i32 faceId) throws (1: ServiceRuntimeException ex1);
  FaceBean getFaceByFeatureId(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  FaceBean getFaceByImageMd5(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  i32 getFaceCount(1: optional string where) throws (1: ServiceRuntimeException ex1);
  list<FaceBean> getFacesByFeatureId(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  list<FaceBean> getFacesByImageMd5(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  FeatureBean getFeature(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  FeatureBean getFeatureByFaceId(1: required i32 faceId) throws (1: ServiceRuntimeException ex1);
  FeatureBean getFeatureByImageMd5(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  i32 getFeatureCount() throws (1: ServiceRuntimeException ex1);
  list<FeatureBean> getFeaturesByImageMd5(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  ImageBean getImage(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  ImageBean getImageByFaceId(1: required i32 faceId) throws (1: ServiceRuntimeException ex1);
  ImageBean getImageByFeatureId(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  binary getImageBytes(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  binary getImageBytesRef(1: optional string primaryKey, 2: optional string refType) throws (1: ServiceRuntimeException ex1);
  i32 getImageCount(1: optional string where) throws (1: ServiceRuntimeException ex1);
  ImageBean getImageRef(1: optional string primaryKey, 2: optional string refType) throws (1: ServiceRuntimeException ex1);
  list<ImageBean> getImagesByFeatureId(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  bool hasFeature(1: optional binary feature) throws (1: ServiceRuntimeException ex1);
  bool hasFeatureByMD5(1: optional string featureId) throws (1: ServiceRuntimeException ex1);
  bool hasImage(1: optional string imageMd5) throws (1: ServiceRuntimeException ex1);
  bool isLocal() throws (1: ServiceRuntimeException ex1);
  list<string> loadFeaturesMd5ByCreateTime(1: optional i64 timestamp) throws (1: ServiceRuntimeException ex1);
  list<string> loadFeaturesMd5ByCreateTimeTimeStr(1: optional string timestamp) throws (1: ServiceRuntimeException ex1);
  list<string> loadFeaturesMd5ByWhere(1: optional string where) throws (1: ServiceRuntimeException ex1);
  list<ImageBean> loadImagesByWhere(1: optional string where, 2: required i32 startRow, 3: required i32 numRows) throws (1: ServiceRuntimeException ex1);
  list<string> loadImagesMd5ByCreateTime(1: optional i64 timestamp) throws (1: ServiceRuntimeException ex1);
  list<string> loadImagesMd5ByCreateTimeTimeStr(1: optional string timestamp) throws (1: ServiceRuntimeException ex1);
  list<string> loadImagesMd5ByWhere(1: optional string where) throws (1: ServiceRuntimeException ex1);
  list<SearchResult> searchFaces(1: optional binary imgData, 2: optional CodeInfo facePos, 3: required double similarty, 4: required i32 rows, 5: optional string where) throws (1: ImageErrorException ex1, 2: NotFaceDetectedException ex2, 3: ServiceRuntimeException ex3);
  list<SearchResult> searchFacesMat(1: optional MatType matType, 2: optional binary matData, 3: required i32 width, 4: required i32 height, 5: optional CodeInfo facePos, 6: required double similarty, 7: required i32 rows, 8: optional string where) throws (1: NotFaceDetectedException ex1, 2: ServiceRuntimeException ex2);
  list<SearchResult> searchFeatures(1: optional binary feature, 2: required double similarty, 3: required i32 rows, 4: optional string where) throws (1: ServiceRuntimeException ex1);
}
