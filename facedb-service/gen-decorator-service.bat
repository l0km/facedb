@rem java -cp lib\codegen-thrift-1.0.3-SNAPSHOT-standalone.jar;facedb-base\target\classes;db\facedb-db-base\target\classes;..\faceapi\faceapi-base\target\classes; ^
@rem     net.gdface.codegen.thrift.ThriftServiceDecoratorGenerator ^
@rem 		-ic net.gdface.facedb.FaceDb ^
@rem 		-rc net.gdface.facedb.FacedbDefaultImpl ^
@rem 		-p net.gdface.facedb.decorator ^
@rem 		-o src\service\java ^
@rem 		--thrift-package net.gdface.facedb.thrift.client ^
@rem 		-gt SERVICE
		
@rem 生成接口的基于facebook/swift的service端实现代码(decorator)
@set sh_folder=%~dp0
@pushd %sh_folder%
@set OUT_FOLDER=src\service\java
@if exist "%OUT_FOLDER%" rmdir  %OUT_FOLDER% /s/q
@call mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate
@popd