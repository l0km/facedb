#!/bin/bash
# 生成 FaceDb javascript(jquery) client代码脚本
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
OUT_FOLDER=$sh_folder/../facedb-client-ext/jquery
# 指定thrift compiler位置
[ $(which thrift) >/dev/null ] && THRIFT_EXE=thrift
[ -z "$THRIFT_EXE" ] && THRIFT_EXE=$sh_folder/dependencies/dist/thrift-$(g++ -dumpmachine)/bin/thrift
$THRIFT_EXE --version || exit

[   -e "$OUT_FOLDER" ] && rm "$OUT_FOLDER/FaceDb*" >/dev/null 2>/dev/null
[ ! -e "$OUT_FOLDER" ] && rm -fr "$OUT_FOLDER"

$THRIFT_EXE --gen js:jquery \
	-out "$OUT_FOLDER" \
	"$sh_folder/FaceDb.thrift"  || exit

## 复制生成的js代码到demo项目
cp -fv "$OUT_FOLDER"/* src/main/resources/META-INF/resources/demo/client/config || exit

popd
