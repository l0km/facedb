@echo off
SETLOCAL
set sh_folder=%~dp0
pushd %sh_folder%
call gen-decorator-service.bat && ^
call mvn install && ^
call gen-thrift.bat && ^
call gen-client.bat && ^
call gen-client-thrifty.bat && ^
call gen-client-jquery.bat && ^
call gen-decorator-client.bat && ^
call gen-decorator-client-thrifty && ^
popd
ENDLOCAL