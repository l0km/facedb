@rem 生成基于Microsoft/thrifty的client端接口实现代码
@set sh_folder=%~dp0
@pushd %sh_folder%..\facedb-client-android
@set OUT_FOLDER=src\client\java
@if exist "%OUT_FOLDER%" rmdir  %OUT_FOLDER% /s/q
@call mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate
@popd
