@set sh_folder=%~dp0
@pushd %sh_folder%..\facedb-client	
set OUT_FOLDER=%sh_folder%\facedb-client\src\thrift\java
if exist %OUT_FOLDER% rmdir  %OUT_FOLDER% /s/q
rem java -jar lib\swift-generator-cli-0.14.2-standalone.jar ^
rem 	FaceDb.thrift ^
rem 	-generate_beans ^
rem  	-override_package net.gdface.facedb.thrift.client ^
rem 	-out %OUT_FOLDER% 

@call mvn com.facebook.mojo:swift-maven-plugin:generate 
@popd