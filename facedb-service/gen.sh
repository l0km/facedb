#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
./gen-decorator-service.sh || exit
mvn install || exit
./gen-thrift.sh || exit
./gen-client.sh || exit
./gen-client-thrifty.sh || exit
./gen-client-jquery.sh || exit
./gen-decorator-client.sh || exit
./gen-decorator-client-thrifty.sh || exit
popd
