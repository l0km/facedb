package net.gdface.service.facedb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.jboss.netty.logging.InternalLoggerFactory;
import org.jboss.netty.logging.Slf4JLoggerFactory;


/**
 * 启动 FaceDb thrift 服务
 * @author guyadong
 *
 */
public class FacedbServiceMain implements FacedbServiceConstants{
	private static final FacedbServiceConfig serviceConfig = FacedbServiceConfig.getInstance();

	public FacedbServiceMain() {
	}
	private static void waitquit(){
		System.out.println("PRESS 'quit' OR 'CTRL-C' to exit");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		try{
			while(!"quit".equalsIgnoreCase(reader.readLine())){				
			}
			System.exit(0);
		} catch (IOException e) {

		}finally {

		}
	}
	public static void main(String ...args){
		Logo.textLogo();
		serviceConfig.parseCommandLine(args);
		// 设置slf4j记录日志,否则会有警告
		InternalLoggerFactory.setDefaultFactory(new Slf4JLoggerFactory());
		FacedbService.buildService().startAsync();
		if(serviceConfig.isXhrStart()){
			FacedbService.buildHttpService().startAsync();
		}
		if(serviceConfig.isRestfulStart()){
			FacedbService.startRestfulService();
		}
		waitquit();
	}
	
}
