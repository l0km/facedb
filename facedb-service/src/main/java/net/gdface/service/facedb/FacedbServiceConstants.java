package net.gdface.service.facedb;

import com.gitee.l0km.com4j.cli.ThriftServiceConstants;

/**
 * 常量定义
 * @author guyadong
 *
 */
public interface FacedbServiceConstants extends ThriftServiceConstants {
	public static final String FACEAPI_CLASS_OPTION_LONG ="faceapi";
	public static final String FACEAPI_CLASS_OPTION_DESC ="faceapi implemention class name,default: empty";
	public static final String FACEAPI_STATIC_OPTION_LONG = "faceapiStaticMethod";
	public static final String FACEAPI_STATIC_OPTION_DESC = "static method name for get instance from faceapi class,default: getInstance";
	public static final String FACEAPI_CONCURRENCY_OPTION ="cc";
	public static final String FACEAPI_CONCURRENCY_OPTION_LONG ="concurrency";
	public static final String FACEAPI_CONCURRENCY_OPTION_DESC ="SDK concurrency capacity,dependent on the sdk implementation,default:count of available processors";

	public static final String FACEDB_CLASS_OPTION_LONG = "facedb";
	public static final String FACEDB_FULL_CLASS_OPTION_DESC = "facedb implemention class name,default: empty";
	public static final String FACEDB_STATIC_OPTION_LONG = "facedbStaticMethod";
	public static final String FACEDB_STATIC_OPTION_DESC = "static method name for get instance from facedb class,default: getInstance";
	public static final String FACEDB_XHR_OPTION ="xp";
	public static final String FACEDB_XHR_OPTION_LONG ="xhr";
	public static final String FACEDB_XHR_OPTION_DESC ="XHR(XML Http Request) port,default:";
	public static final String FACEDB_XHR_START_OPTION ="xs";
	public static final String FACEDB_XHR_START_OPTION_LONG ="xstart";
	public static final String FACEDB_XHR_START_OPTION_DESC ="start XHR(XML Http Request) service";
	public static final String FACEDB_RESTFUL_OPTION ="rp";
	public static final String FACEDB_RESTFUL_OPTION_LONG ="restful";
	public static final String FACEDB_RESTFUL_OPTION_DESC ="RESTful port,default:";
	public static final String FACEDB_RESTFUL_START_OPTION ="rs";
	public static final String FACEDB_RESTFUL_START_OPTION_LONG ="rstart";
	public static final String FACEDB_RESTFUL_START_OPTION_DESC ="start RESTful(spring) service";
	public static final String FACEDB_SWAGGER_ENABLE_OPTION ="sw";
	public static final String FACEDB_SWAGGER_ENABLE_OPTION_LONG ="swagger";
	public static final String FACEDB_SWAGGER_ENABLE_OPTION_DESC ="start swagger document";
	public static final String FACEDB_CORS_ENABLE_OPTION ="co";
	public static final String FACEDB_CORS_ENABLE_OPTION_LONG ="cors";
	public static final String FACEDB_CORS_ENABLE_OPTION_DESC ="enable CORS(Cross-origin resource sharing) for RESTful(spring) service";
}
