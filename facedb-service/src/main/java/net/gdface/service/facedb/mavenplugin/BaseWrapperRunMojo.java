package net.gdface.service.facedb.mavenplugin;

import java.util.ArrayList;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import net.gdface.facedb.CommonConstant;
import net.gdface.service.facedb.FacedbServiceConstants;

/**
 * maven 插件基类<br>
 * 执行FaceDb RPC服务启动
 * 
 * @author guyadong
 *
 */
@Mojo(name = "run", requiresProject = false)
public abstract class BaseWrapperRunMojo extends AbstractMojo implements FacedbServiceConstants,CommonConstant{
	/**
	 * Set this to true to allow Maven to continue to execute after invoking the
	 * run goal.
	 *
	 */
	@Parameter(property = "maven.service.fork", defaultValue = "false")
	private boolean fork;
	
	/**
	 * FaceApi并发能力<br>
	 * SDK concurrency capacity,dependent on the sdk implementation,default:count of available processors
	 */
	@Parameter(property="maven.faceapi.concurrency")
	private int concurrency = Runtime.getRuntime().availableProcessors();
	/**
	 * FaceDb服务端口号<br>
	 * FaceDb service port number,default value: 26431
	 */
	@Parameter(property="maven.thrift.servicePort")
	private int servicePort = DEFAULT_PORT;
	/**
	 * 工作线程数量<br>
	 * work thread number,default: count of available processors
	 */
	@Parameter(property="maven.thrift.workThreads")
	private int workThreads = Runtime.getRuntime().availableProcessors();
	/**
	 * 连接上限<br>
	 * an upper bound on the number of concurrent connections the server will accept.default:32
	 */
	@Parameter(property="maven.thrift.connectionLimit")
	private int connectionLimit = DEFAULT_CONNECTION_LIMIT;
	/**
	 * 空闲连接超时(秒)<br>
	 * Sets a timeout period between receiving requests from a client connection. 
	 * If the timeout is exceeded (no complete requests have arrived from the client within the timeout), 
	 * the server will disconnect the idle client.default: 60s
	 */
	@Parameter(property="maven.thrift.idleConnectionTimeout")
	private int idleConnectionTimeout = DEFAULT_IDLE_TIMEOUT;
	/**
	 * 启动XHR服务<br>
	 * start XHR(XML Http Request) service
	 */
	@Parameter(property="maven.thrift.xhrEnable")
	private boolean xhrEnable;
	/**
	 * XHR服务端口<br>
	 * XHR(XML Http Request) port,default:26432
	 */
	@Parameter(property="maven.thrift.xhrPort")
	private int xhrPort = DEFAULT_PORT_XHR;
	/**
	 * 启动RESTful服务(start RESTful service)<br>
	 */
	@Parameter(property="maven.thrift.restfulEnable")
	private boolean restfulEnable;
	/**
	 * RESTfu服务端口<br>
	 * RESTful service port,default: 26433
	 */
	@Parameter(property="maven.thrift.restfulPort")
	private int restfulPort = DEFAULT_PORT_RESTFUL;
	/**
	 * 启用swagger文档<br>
	 * start swagger document
	 */
	@Parameter(property="maven.thrift.swaggerEnable")
	private boolean swaggerEnable;
	/**
	 * 启用RESTful服务跨域(CORS)访问<br>
	 * enable CORS(Cross-origin resource sharing) for RESTful(spring) service
	 */
	@Parameter(property="maven.thrift.corsEnable")
	private boolean corsEnable;

	public BaseWrapperRunMojo() {
	}
	abstract protected void doExecute();
	@Override
	public final void execute() throws MojoExecutionException, MojoFailureException {
		try{
			doExecute();
		}catch(Exception e){
			throw new MojoExecutionException(e.getMessage(),e);
		}
		if (!fork) {
			waitIndefinitely();
		}
	}
	protected String[] makeArgs(){
		ArrayList<String> list = new ArrayList<String>(16);
		list.add("--" + FACEAPI_CONCURRENCY_OPTION_LONG);
		list.add(Integer.toString(concurrency));
		list.add("--" + SERVICE_PORT_OPTION_LONG);
		list.add(Integer.toString(servicePort));
		list.add("--" + WORK_THREADS_OPTION_LONG);
		list.add(Integer.toString(workThreads));
		list.add("--" + CONNECTION_LIMIT_OPTION_LONG);
		list.add(Integer.toString(connectionLimit));
		list.add("--" + IDLE_CONNECTION_TIMEOUT_OPTION_LONG);
		list.add(Integer.toString(idleConnectionTimeout));
		list.add("--" + FACEDB_XHR_START_OPTION_LONG);
		list.add("--" + FACEDB_XHR_OPTION_LONG);
		list.add(Integer.toString(xhrPort));
		list.add("--" + FACEDB_RESTFUL_START_OPTION_LONG);
		list.add("--" + FACEDB_RESTFUL_OPTION_LONG);
		list.add(Integer.toString(restfulPort));
		list.add("--" + FACEDB_SWAGGER_ENABLE_OPTION_LONG);
		list.add("--" + FACEDB_CORS_ENABLE_OPTION_LONG);
		return list.toArray(new String[list.size()]);
	}
	/**
	 * Causes the current thread to wait indefinitely. This method does not
	 * return.
	 */
	private void waitIndefinitely() {
		Object lock = new Object();
		synchronized (lock) {
			try {
				lock.wait();
			} catch (InterruptedException e) {
				getLog().warn("RunMojo.interrupted", e);
			}
		}
	}
}
