package net.gdface.service.facedb;

import java.util.LinkedHashMap;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.gitee.l0km.com4j.basex.ReflectionUtils;
import com.gitee.l0km.com4j.cli.ThriftServiceConfig;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import net.gdface.facedb.CommonConstant;
import net.gdface.facedb.FaceDb;
import net.gdface.facedb.FacedbFull;
import net.gdface.facedb.FacedbLocal;
import net.gdface.sdk.ContextLoader;
import net.gdface.sdk.FaceApi;
import net.gdface.sdk.FaceApiContext;
import net.gdface.sdk.ContextLoader.GlobalContextField;

/**
 * 服务配置参数
 * @author guyadong
 *
 */
public class FacedbServiceConfig extends ThriftServiceConfig implements FacedbServiceConstants,CommonConstant {

	private FaceDb faceDb;
	private int faceapiConcurrency;
	private int xhrPort;
	private int restfulPort;
	private boolean xhrStart;
	private boolean restfulStart;
	private boolean swaggerEnable = true;
	private boolean corsEnable = true;

	private static final  FacedbServiceConfig INSTANCE = new FacedbServiceConfig();
	public FacedbServiceConfig() {
		super(DEFAULT_PORT);
		options.addOption(Option.builder().longOpt(FACEAPI_CLASS_OPTION_LONG)
				.desc(FACEAPI_CLASS_OPTION_DESC).numberOfArgs(1).build());
		options.addOption(Option.builder().longOpt(FACEAPI_STATIC_OPTION_LONG)
				.desc(FACEAPI_STATIC_OPTION_DESC).numberOfArgs(1).build());
		options.addOption(Option.builder(FACEAPI_CONCURRENCY_OPTION).longOpt(FACEAPI_CONCURRENCY_OPTION_LONG)
				.desc(FACEAPI_CONCURRENCY_OPTION_DESC).numberOfArgs(1).type(Number.class).build());
		options.addOption(Option.builder().longOpt(FACEDB_CLASS_OPTION_LONG)
				.desc(FACEDB_FULL_CLASS_OPTION_DESC).numberOfArgs(1).build());
		options.addOption(Option.builder().longOpt(FACEDB_STATIC_OPTION_LONG)
				.desc(FACEDB_STATIC_OPTION_DESC).numberOfArgs(1).build());

		options.addOption(Option.builder(FACEDB_XHR_OPTION).longOpt(FACEDB_XHR_OPTION_LONG)
				.desc(FACEDB_XHR_OPTION_DESC + DEFAULT_PORT_XHR).numberOfArgs(1).type(Number.class).build());
		options.addOption(Option.builder(FACEDB_XHR_START_OPTION).longOpt(FACEDB_XHR_START_OPTION_LONG)
				.desc(FACEDB_XHR_START_OPTION_DESC).numberOfArgs(0).build());
		options.addOption(Option.builder(FACEDB_RESTFUL_OPTION).longOpt(FACEDB_RESTFUL_OPTION_LONG)
				.desc(FACEDB_RESTFUL_OPTION_DESC + DEFAULT_PORT_RESTFUL).numberOfArgs(1).type(Number.class).build());
		options.addOption(Option.builder(FACEDB_RESTFUL_START_OPTION).longOpt(FACEDB_RESTFUL_START_OPTION_LONG)
				.desc(FACEDB_RESTFUL_START_OPTION_DESC).numberOfArgs(0).build());
		options.addOption(Option.builder(FACEDB_SWAGGER_ENABLE_OPTION).longOpt(FACEDB_SWAGGER_ENABLE_OPTION_LONG)
				.desc(FACEDB_SWAGGER_ENABLE_OPTION_DESC).numberOfArgs(0).build());
		options.addOption(Option.builder(FACEDB_CORS_ENABLE_OPTION).longOpt(FACEDB_CORS_ENABLE_OPTION_LONG)
				.desc(FACEDB_CORS_ENABLE_OPTION_DESC).numberOfArgs(0).build());

		defaultValue.setProperty(FACEAPI_CLASS_OPTION_LONG,"");
		defaultValue.setProperty(FACEAPI_STATIC_OPTION_LONG,DEFAULT_STATIC_METHOD);
		defaultValue.setProperty(FACEAPI_CONCURRENCY_OPTION_LONG,Runtime.getRuntime().availableProcessors());
		defaultValue.setProperty(FACEDB_CLASS_OPTION_LONG,"");
		defaultValue.setProperty(FACEDB_STATIC_OPTION_LONG,DEFAULT_STATIC_METHOD);
		defaultValue.setProperty(FACEDB_XHR_OPTION_LONG,DEFAULT_PORT_XHR);
		defaultValue.setProperty(FACEDB_XHR_START_OPTION_LONG,false);
		defaultValue.setProperty(FACEDB_RESTFUL_OPTION_LONG,DEFAULT_PORT_RESTFUL);
		defaultValue.setProperty(FACEDB_RESTFUL_START_OPTION_LONG,false);
		defaultValue.setProperty(FACEDB_SWAGGER_ENABLE_OPTION_LONG,true);	
		defaultValue.setProperty(FACEDB_CORS_ENABLE_OPTION_LONG,true);		

	}

	@Override
	public void loadConfig(Options options, CommandLine cmd) throws ParseException {
		super.loadConfig(options, cmd);
		this.faceapiConcurrency = ((Number)getProperty(FACEAPI_CONCURRENCY_OPTION_LONG)).intValue();
		this.faceDb = getFaceDbInstance();
		this.xhrStart = (Boolean)getProperty(FACEDB_XHR_START_OPTION_LONG);
		this.xhrPort = ((Number)getProperty(FACEDB_XHR_OPTION_LONG)).intValue();
		this.restfulStart = (Boolean)getProperty(FACEDB_RESTFUL_START_OPTION_LONG);
		this.restfulPort = ((Number)getProperty(FACEDB_RESTFUL_OPTION_LONG)).intValue();
		this.swaggerEnable = (Boolean)getProperty(FACEDB_SWAGGER_ENABLE_OPTION_LONG);
		this.corsEnable = (Boolean)getProperty(FACEDB_CORS_ENABLE_OPTION_LONG);

	}
	private FaceDb getFaceDbInstance(){
		FaceApi faceApi = getFaceApiInstance();
		LinkedHashMap<Class<?>,Object>ctorArgs = Maps.newLinkedHashMap();
		String faceDbClassName = (String)getProperty(FACEDB_CLASS_OPTION_LONG);
		if(null != faceApi){
			logger.info("FaceApi Instance:" + faceApi.getClass().getSimpleName());
			if(faceDbClassName.isEmpty()){
				faceDbClassName = FacedbFull.class.getName();
			}
			ctorArgs.put(FaceApi.class, faceApi);
		}else{
			if(faceDbClassName.isEmpty()){
				faceDbClassName = FacedbLocal.class.getName();
			}
		}
		ImmutableMap<String, Object> params = ImmutableMap.<String, Object>of(
				ReflectionUtils.PROP_CLASSNAME, faceDbClassName, 
				ReflectionUtils.PROP_STATICMETHODNAME, (String)getProperty(FACEDB_STATIC_OPTION_LONG),
				ReflectionUtils.PROP_CONSTRUCTORPARAMS,ctorArgs);
		try{
			return ReflectionUtils.getInstance(FaceDb.class, params);
		} catch (Exception e) {
			Throwables.throwIfUnchecked(e);
			throw new RuntimeException(e);
		}
	}
	/**
	 * 返回{@link FaceApi}实例<br>
	 * 如果定义了命令行参数 {@link FacedbServiceConstants#FACEAPI_CLASS_OPTION_LONG}则从该参数获取{@link FaceApi}实例<br>
	 * 否则尝试从从SPI加载的上下文实例({@link FaceApiContext})中获取获取{@link FaceApi}实例,获取不到返回{@code null}
	 * @return {@link FaceApi}实例
	 */
	private FaceApi getFaceApiInstance(){
		// 设置并发线程数
		ContextLoader.setGlobalContext(GlobalContextField.CONCURRENCY, faceapiConcurrency);
		String clazzName = (String)getProperty(FACEAPI_CLASS_OPTION_LONG);
		if(clazzName.isEmpty()){
			FaceApiContext context = ContextLoader.getInstance().getContext();
			return context == null ? null : (FaceApi) context.getContext().get(ContextLoader.ContextField.INSTANCE);
		}
		ImmutableMap<String, Object> params = ImmutableMap.<String, Object>of(ReflectionUtils.PROP_CLASSNAME, clazzName, 
				ReflectionUtils.PROP_STATICMETHODNAME, (String)getProperty(FACEAPI_STATIC_OPTION_LONG));
		try {
			return ReflectionUtils.getInstance(FaceApi.class, params);
		} catch (Exception e) {
			Throwables.throwIfUnchecked(e);
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getAppName() {
		return FacedbServiceMain.class.getName();
	}

	public static FacedbServiceConfig getInstance() {
		return INSTANCE;
	}
	
	public FaceDb getFaceDb() {
		return faceDb;
	}

	/**
	 * @return xhrPort
	 */
	public int getXhrPort() {
		return xhrPort;
	}

	/**
	 * @return restfulPort
	 */
	public int getRestfulPort() {
		return restfulPort;
	}

	/**
	 * @return xhrStart
	 */
	public boolean isXhrStart() {
		return xhrStart;
	}

	/**
	 * @return restfulStart
	 */
	public boolean isRestfulStart() {
		return restfulStart;
	}

	public boolean isSwaggerEnable() {
		return swaggerEnable;
	}

	public boolean isCorsEnable() {
		return corsEnable;
	}

	public void setCorsEnable(boolean corsEnable) {
		this.corsEnable = corsEnable;
	}
	
}
