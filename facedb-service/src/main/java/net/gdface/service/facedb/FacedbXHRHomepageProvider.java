package net.gdface.service.facedb;

import java.nio.charset.Charset;

import com.gitee.l0km.xthrift.service.XHRHomepageProvider;
import com.google.common.io.Resources;

import net.gdface.facedb.Version;

public class FacedbXHRHomepageProvider implements XHRHomepageProvider {

	@Override
	public byte[] homepage() throws Exception {
		String homepage = Resources.toString(FacedbXHRHomepageProvider.class.getResource("/facedb_homepage.html"), 
				Charset.forName("utf-8"));
		return homepage.replaceAll("\\$\\{version\\}", Version.VERSION).getBytes();
	}

}
