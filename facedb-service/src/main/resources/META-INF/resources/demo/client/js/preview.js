imgNatWid = 0;   //定义原照片的宽度
// imgBlob = '';    //定义照片的二进制数据流
// 上传照片
function previewImage(file) {
  var prevDiv = $(file).siblings('.preview')[0];
  var files = $(file)[0].files[0];
  var type = files.type;

  if (type != 'image/jpg' && type != 'image/jpeg' && type != 'image/png' && type != 'image/gif' && type != 'image/bmp') {
    $('.box').show().delay(3000).fadeOut();
    $('.box').find('.msg').text('只能上传图片文件（.jpg .jpeg .png .gif）');
    return false;
  }
  if (file.files && file.files[0]) {
    var reader = new FileReader();
    reader.onload = function (evt) {
      prevDiv.innerHTML = '<img src="' + evt.target.result + '" />';

      var img = new Image();
      img.src = reader.result;
      //获取照片原尺寸
      img.onload = function () {
        imgNatWid = this.width;
        imgNatHei = this.height;
      };
    }

    
    reader.readAsDataURL(file.files[0]);
    readBlob($(file), prevDiv);
  }
}

// 读取图像文件的二进制数据
function readBlob(files, prevDiv) {
  var file = files[0].files[0];
  var reader = new FileReader();
  reader.onloadend = function (evt) {
    if (evt.target.readyState == FileReader.DONE) { // DONE == 2
      imgBlob = evt.target.result;
    }


    // 判断是否上传单人脸照片
    var id = prevDiv.getAttribute('id');
    if(id == null){
      try {
        var code = client.detectAndGetCodeInfo(imgBlob);
        if(code.length>1){
          prevDiv.innerHTML = '<img src="../style/images/icon1.png" />';
          $('.box').show().delay(3000).fadeOut();
          $('.box').find('.msg').text('请上传单人脸照片');
        }
      } catch (error) {
        console.log(error)
      }
    }
  };
  reader.readAsBinaryString(file);
}