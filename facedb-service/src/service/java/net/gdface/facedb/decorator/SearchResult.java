// ______________________________________________________
// Generated by codegen - https://gitee.com/l0km/codegen 
// template: thrift/service/perstruct/bean.decorator.class.vm
// ______________________________________________________

package net.gdface.facedb.decorator;
import com.gitee.l0km.xthrift.base.ThriftDecorator;
import java.nio.ByteBuffer;
import com.facebook.swift.codec.ThriftStruct;
import com.facebook.swift.codec.ThriftField;
import com.facebook.swift.codec.ThriftField.Requiredness;
import com.gitee.l0km.xthrift.thrift.TypeTransformer;
/**

 * ========================================<br>
 * decorator pattern 装饰者模式代理{@link net.gdface.facedb.SearchResult}<br>
 * 代理对象必须有默认构造方法,必须是普通类型,不可以有@ThriftStruct注释<br>
 * 转发所有{@link net.gdface.facedb.SearchResult}get/set方法到{@link #delegate()}指定的实例,<br>
 * 计算机生成代码(generated by automated tools ThriftServiceDecoratorGenerator @author guyadong)<br>
 *
 */
@ThriftStruct
public final class SearchResult implements ThriftDecorator<net.gdface.facedb.SearchResult> {
    private final net.gdface.facedb.SearchResult delegate;
    
    public SearchResult(){
        this(new net.gdface.facedb.SearchResult());
    }
    public SearchResult(net.gdface.facedb.SearchResult delegate) {
        if(null == delegate){
            throw new NullPointerException("delegate is null");
        }
        // 检查被装饰类是否有@ThriftStruct注释
        if(delegate.getClass().isAnnotationPresent(ThriftStruct.class)){
            throw new IllegalArgumentException(
                String.format("NOT ALLOW %s have @ThriftStruct annotation",delegate.getClass().getName()));
        }
        this.delegate = delegate;
    }

    /**
     * 返回被装饰的{@link net.gdface.facedb.SearchResult}实例
     */
    @Override
    public net.gdface.facedb.SearchResult delegate() {
        return delegate;
    }
    @Override
    public int hashCode() {
        return delegate().hashCode();
    }
    @Override
    public boolean equals(Object obj) {
        return delegate().equals(obj);
    }
    @Override
    public String toString() {
        return delegate().toString();
    }
    @ThriftField(value = 1,name = "featureId",requiredness=Requiredness.OPTIONAL)
    public ByteBuffer getFeatureId(){
        return TypeTransformer.getInstance().to(
                    delegate().getFeatureId(),
                    byte[].class,
                    ByteBuffer.class);
    }
    
    @ThriftField
    public void setFeatureId(ByteBuffer featureId){
        delegate().setFeatureId(TypeTransformer.getInstance().to(
                    featureId,
                    ByteBuffer.class,
                    byte[].class));
    }

    /**
     * 返回特征码ID(32字符HEX字符串)
     * @return 特征码ID
     */
    @ThriftField(value = 2,name = "hexFeatureId",requiredness=Requiredness.OPTIONAL)
    public String getHexFeatureId(){
        return delegate().getHexFeatureId();
    }
    
    /**
     * @param hexFeatureId 特征码ID(32字符HEX字符串)
     */
    @ThriftField
    public void setHexFeatureId(String hexFeatureId){
        delegate().setHexFeatureId(hexFeatureId);
    }

    @ThriftField(value = 3,name = "owner",requiredness=Requiredness.OPTIONAL)
    public String getOwner(){
        return delegate().getOwner();
    }
    
    @ThriftField
    public void setOwner(String owner){
        delegate().setOwner(owner);
    }

    @ThriftField(value = 4,name = "similarity",requiredness=Requiredness.REQUIRED)
    public double getSimilarity(){
        return delegate().getSimilarity();
    }
    
    @ThriftField
    public void setSimilarity(double similarity){
        delegate().setSimilarity(similarity);
    }

}