// ______________________________________________________
// Generated by codegen - https://gitee.com/l0km/codegen 
// template: thrift/service/perstruct/bean.decorator.class.vm
// ______________________________________________________

package net.gdface.sdk.decorator;
import com.gitee.l0km.xthrift.base.ThriftDecorator;
import com.facebook.swift.codec.ThriftStruct;
import com.facebook.swift.codec.ThriftField;
import com.facebook.swift.codec.ThriftField.Requiredness;
/**
 * 通用矩形定义对象
 * @author guyadong

 * ========================================<br>
 * decorator pattern 装饰者模式代理{@link net.gdface.sdk.FRect}<br>
 * 代理对象必须有默认构造方法,必须是普通类型,不可以有@ThriftStruct注释<br>
 * 转发所有{@link net.gdface.sdk.FRect}get/set方法到{@link #delegate()}指定的实例,<br>
 * 计算机生成代码(generated by automated tools ThriftServiceDecoratorGenerator @author guyadong)<br>
 *
 */
@ThriftStruct
public final class FRect implements ThriftDecorator<net.gdface.sdk.FRect> {
    private final net.gdface.sdk.FRect delegate;
    
    public FRect(){
        this(new net.gdface.sdk.FRect());
    }
    public FRect(net.gdface.sdk.FRect delegate) {
        if(null == delegate){
            throw new NullPointerException("delegate is null");
        }
        // 检查被装饰类是否有@ThriftStruct注释
        if(delegate.getClass().isAnnotationPresent(ThriftStruct.class)){
            throw new IllegalArgumentException(
                String.format("NOT ALLOW %s have @ThriftStruct annotation",delegate.getClass().getName()));
        }
        this.delegate = delegate;
    }

    /**
     * 返回被装饰的{@link net.gdface.sdk.FRect}实例
     */
    @Override
    public net.gdface.sdk.FRect delegate() {
        return delegate;
    }
    @Override
    public int hashCode() {
        return delegate().hashCode();
    }
    @Override
    public boolean equals(Object obj) {
        return delegate().equals(obj);
    }
    @Override
    public String toString() {
        return delegate().toString();
    }
    @ThriftField(value = 1,name = "height",requiredness=Requiredness.REQUIRED)
    public int getHeight(){
        return delegate().getHeight();
    }
    
    @ThriftField
    public void setHeight(int height){
        delegate().setHeight(height);
    }

    @ThriftField(value = 2,name = "left",requiredness=Requiredness.REQUIRED)
    public int getLeft(){
        return delegate().getLeft();
    }
    
    @ThriftField
    public void setLeft(int left){
        delegate().setLeft(left);
    }

    @ThriftField(value = 3,name = "top",requiredness=Requiredness.REQUIRED)
    public int getTop(){
        return delegate().getTop();
    }
    
    @ThriftField
    public void setTop(int top){
        delegate().setTop(top);
    }

    @ThriftField(value = 4,name = "width",requiredness=Requiredness.REQUIRED)
    public int getWidth(){
        return delegate().getWidth();
    }
    
    @ThriftField
    public void setWidth(int width){
        delegate().setWidth(width);
    }

}