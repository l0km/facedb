package net.gdface.facedb.service;

import java.net.URLConnection;

import org.junit.Test;
import org.springframework.http.MediaType;

public class MediaTypeTest {

	@Test
	public void test() {
	    String bmptype = URLConnection.guessContentTypeFromName(".bmp");
	    System.out.printf("bmp type =%s\n", bmptype);
		MediaType mediaType = MediaType.parseMediaType(bmptype);
		System.out.printf("bmp type =%s\n", mediaType.toString());
	}

}
