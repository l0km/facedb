@rem java -cp lib\codegen-thrift-1.0.3-SNAPSHOT-standalone.jar;facedb-base\target\classes;db\facedb-db-base\target\classes;..\faceapi\faceapi-base\target\classes; ^
@rem     net.gdface.codegen.thrift.ThriftServiceDecoratorGenerator ^
@rem 		-ic net.gdface.facedb.FaceDb ^
@rem 		-rc net.gdface.facedb.FacedbDefaultImpl ^
@rem 		-p net.gdface.facedb.thrift ^
@rem 		-o facedb-client\src\client\java ^
@rem 		--thrift-package net.gdface.facedb.thrift.client ^
@rem 		-gt CLIENT

@rem 生成基于facebook/swift的client端接口实现代码@set sh_folder=%~dp0
@pushd %sh_folder%..\facedb-client
@set OUT_FOLDER=src\client\java
@if exist "%OUT_FOLDER%" rmdir  %OUT_FOLDER% /s/q
@call mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate
@popd