echo off
SETLOCAL
rem 生成 FaceDb javascript(jquery) client代码脚本
set sh_folder=%~dp0
rem 删除最后的 '\'
set sh_folder=%sh_folder:~0,-1%
pushd "%sh_folder%"
set OUT_FOLDER=%sh_folder%\..\facedb-client-ext\jquery
:: 指定thrift compiler位置
where thrift >nul 2>nul
if errorlevel 1 (
	echo thrift NOT FOUND.
	exit /B -1
)
set THRIFT_EXE=thrift
if exist "%OUT_FOLDER%" (
	del  "%OUT_FOLDER%"\FaceDb* >nul 2>nul
	)
if not exist "%OUT_FOLDER%" mkdir  "%OUT_FOLDER%"

%THRIFT_EXE% --gen js:jquery^
	-out "%OUT_FOLDER%" ^
	%sh_folder%\..\facedb-service\FaceDb.thrift || exit /B -1
:: 复制生成的js代码到demo项目
XCOPY /Y "%OUT_FOLDER%"\* src\main\resources\META-INF\resources\demo\client\config
popd
ENDLOCAL