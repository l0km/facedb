#!/bin/bash
# Java 8 required
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder/../facedb-client-android
OUT_FOLDER=src/thrifty/java
[ -e "$OUT_FOLDER" ] && rm -fr "$OUT_FOLDER"
mvn com.gitee.l0km:thrifty-compiler-maven-plugin:stub || exit
popd