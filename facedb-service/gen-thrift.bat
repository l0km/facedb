rem java -cp lib\idl-generator-cli-1.5-standalone.jar;facedb-base\target\classes;db\facedb-db-base\target\classes;facedb-service\target\classes;..\common-java\common-thrift\target\classes;..\faceapi\faceapi-base\target\classes;..\common-java\com4j-base\target\classes ^
rem     com.gitee.l0km.thrift.swift2thrift.generator.Main ^
rem     -namespace cpp gdface ^
rem     -namespace java net.gdface.facedb.thrift.client ^
rem     -namespace py gdface.thrift ^
rem     -out FaceDb.thrift ^
rem     -package net.gdface.facedb.decorator FaceDbThriftDecorator ^
rem     -recursive

@set sh_folder=%~dp0
@pushd %sh_folder%..\facedb-service
mvn com.gitee.l0km:swift2thrift-maven-plugin:generate
@popd