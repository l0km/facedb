package net.gdface.facedb.client;

import static com.gitee.l0km.com4j.base.SimpleLog.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.gitee.l0km.com4j.base.BinaryUtils;
import com.gitee.l0km.ximage.ImageErrorException;
import com.gitee.l0km.xthrift.thrifty.ClientFactory;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import net.gdface.facedb.CapcityFieldConstant;
import net.gdface.facedb.DuplicateRecordException;
import net.gdface.facedb.FaceDbDecorator;
import net.gdface.facedb.FaceDbGenericDecorator;
import net.gdface.facedb.db.FeatureBean;
import net.gdface.facedb.db.ImageBean;
import net.gdface.facedb.thrift.FaceDbThriftClient;
import net.gdface.sdk.CodeInfo;
import net.gdface.sdk.FaceApiGenericDecorator;
import net.gdface.sdk.NotFaceDetectedException;

/**
 * FaceDb 接口测试
 * @author guyadong
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FacedbTest implements CapcityFieldConstant{
	private static FaceDbGenericDecorator facedb;
	private static FaceApiGenericDecorator faceapi;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		FaceDbDecorator thriftInstance = ClientFactory
				.builder()
				.setHostAndPort("127.0.0.1",26431)
				.build(FaceDbThriftClient.class, FaceDbDecorator.class);
		facedb= new FaceDbGenericDecorator(thriftInstance);
		// 提供本地FaceApi实例
		//faceapi= new FaceApiGenericDecorator(net.gdface.cassdk.FaceApiCas.getInstance());
	}

	@Test
	public void test1DetectAndAddFeatures() throws IOException {
		byte[] imgData = BinaryUtils.getBytesNotEmpty(FacedbTest.class.getResourceAsStream("/images/he049.jpg"));
		String imageMd5 = BinaryUtils.getMD5String(imgData);
		try {
			if(facedb.hasImage(imageMd5)){
				// 测试deleteImage
				boolean b = facedb.deleteImage(imageMd5,true);
				assertTrue(b);
			}
			assertFalse(facedb.deleteImage(imageMd5,true));
			// 测试detectAndAddFeatures
			{
				ImageBean imageBean = facedb.detectAndAddFeatures(imgData, 0);
				log("imageBean = {}", imageBean);
				byte[] imgBytes = facedb.getImageBytes(imageMd5);
				boolean equ = ByteBuffer.wrap(imgData).equals(ByteBuffer.wrap(imgBytes));
				log("image bytes equal = {}",equ);
			}
			// 测试 DuplicateRecordException异常
			{
				try{
					@SuppressWarnings("unused")
					ImageBean imageBean = facedb.detectAndAddFeatures(imgData, 0);
					fail("show throw exception");
				}catch(DuplicateRecordException e){
					log(e.getMessage());
				}
			}
			// 测试 getCodeInfo getFeatures
			{
				List<FeatureBean> featureBeans = facedb.getFeaturesByImageMd5(imageMd5);
				assertTrue(featureBeans.size() ==5);
				int count = 0;
				for(FeatureBean feature:featureBeans){
					log("featureBean {}= {}",++count,feature);
					log("codeinfo {}= {}",count,facedb.getCodeInfoByFeatureId(feature.getMd5()));
				}
			}
			// 测试 getCodeInfosByImageMd5
			{
				List<CodeInfo> codes = facedb.getCodeInfosByImageMd5(imageMd5);
				assertTrue(codes.size() ==5);
				int count = 0;
				for(CodeInfo code:codes){
					log("code {}= {}",++count,code);
				}
			}
			// 测试deleteImage
			boolean b = facedb.deleteImage(imageMd5,true);
			assertTrue(b);
		
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		} 
	}
	@Test
	public void test2ImageError(){
		byte[] errorImage = new byte[]{1,23,3};
		try {
			facedb.detectAndAddFeatures(errorImage, 0);
			assertTrue(false);
		} catch (ImageErrorException e) {
			// 测试ImageErrorException
			log(e.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		} 
	}
	@Test
	public void test3NotFaceDetectedException() throws  IOException{
		byte[] imgData = BinaryUtils.getBytesNotEmpty(FacedbTest.class.getResourceAsStream("/images/timg01.jpg"));
		try {
			facedb.detectAndAddFeatures(imgData, 1);
			assertTrue(false);
		} catch (NotFaceDetectedException e) {
			// 测试NotFaceDetectedException
			log(e.toString());
			assertTrue(true);
		}catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		} 
	}
	@Test
	public void test4Compare() throws IOException{
		byte[] imgData = BinaryUtils.getBytesNotEmpty(FacedbTest.class.getResourceAsStream("/images/guyadong-1.jpg"));
		String imageMd5 = BinaryUtils.getMD5String(imgData);
		try {
			if(facedb.hasImage(imageMd5)){
				facedb.deleteImage(imageMd5, true);
				log("delete image {}",imageMd5);
			}
			ImageBean imageBean1 = facedb.detectAndAddFeatures(imgData, 1);
			FeatureBean featureBean1 = facedb.getFeatureByImageMd5(imageBean1.getMd5());
			{
				// 单人比较测试
				Map<CodeInfo, Double> result = facedb.detectAndCompareFaces(featureBean1.getMd5(), FacedbTest.class.getResourceAsStream("/images/guyadong-2.jpg"), 1).asMap();
				int count = 0;
				for( Entry<CodeInfo, Double> entry:result.entrySet()){
					log("entry {} simility: {}  {}",++count,entry.getValue(),entry.getKey().getPos());
				}
			}
			{
				// 多人比较测试
				Map<CodeInfo, Double> result = facedb.detectAndCompareFaces(featureBean1.getMd5(), FacedbTest.class.getResourceAsStream("/images/he049.jpg"), 0).asMap();
				int count = 0;
				for( Entry<CodeInfo, Double> entry:result.entrySet()){
					log("entry {} simility: {}  {}",++count,entry.getValue(),entry.getKey().getPos());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		} finally{
			boolean b = facedb.deleteImage(imageMd5, true);
			assertTrue(b);			
		}
	}
	/**
	 * 本地 FaceApi 检测人脸,提交 FaceDb 保存特征到数据库<br>
	 * 有提供FaceApi 实例时才能执行
	 * @throws IOException
	 */
	//@Test
	public void test5AddImage() throws IOException{
		byte[] imgData = BinaryUtils.getBytesNotEmpty(FacedbTest.class.getResourceAsStream("/images/guyadong-1.jpg"));
		String imageMd5 = BinaryUtils.getMD5String(imgData);
		if(facedb.hasImage(imageMd5)){
			facedb.deleteImage(imageMd5, true);
			log("delete image {}",imageMd5);
		}
		try {
			CodeInfo[] codes = faceapi.detectAndGetCodeInfo(imgData, 0);
			ImageBean imageBean = facedb.addImage(imgData, Arrays.asList(codes));
			log("imageBean = {}",imageBean);
			List<FeatureBean> featureBeans = facedb.getFeaturesByImageMd5(imageMd5);
			int count = 0;
			for(FeatureBean feature:featureBeans){
				log("featureBean {}= {}",++count,feature);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		} finally{
			boolean b = facedb.deleteImage(imageMd5, true);
			assertTrue(b);
		}
	}
	@Test
	public void test6Miscellaneous(){
		String supportedSearch = facedb.dbCapacity().get(C_SUPPORT_SEARCH);
		log("supportedSearch={}",supportedSearch);
		boolean isLocal = facedb.isLocal();
		log("isLocal = {}",isLocal);
		// 测试返回null
		ImageBean nullBean = facedb.getImage("hello");
		log("nullBean={}",nullBean);
		assertTrue(null == nullBean);
	}
	/**
	 * 多线程测试
	 * @throws InterruptedException
	 * @throws IOException 
	 * @throws  
	 */
	@Test
	public void test7MultiThreadCompareFace() throws InterruptedException,  IOException{
        final byte[] imgData = BinaryUtils.getBytesNotEmpty(FacedbTest.class.getResourceAsStream("/images/guyadong-1.jpg"));
		String imageMd5 = BinaryUtils.getMD5String(imgData);
		ExecutorService executor = new ThreadPoolExecutor(
                8, 
                8,
                0,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(),
                new ThreadFactoryBuilder()
                    .setNameFormat("cached-pool-%d")
                    .build());
		try {
			if(facedb.hasImage(imageMd5)){
				facedb.deleteImage(imageMd5, true);
				log("delete image {}",imageMd5);
			}

			ImageBean imageBean = facedb.detectAndAddFeatures(imgData, 1);
			final FeatureBean featureBean = facedb.getFeatureByImageMd5(imageBean.getMd5());
			final String[] images = new String[]{
					"/images/guyadong-1.jpg",
					"/images/guyadong-2.jpg",
					"/images/guyadong-3.jpg",
					"/images/guyadong-4.jpg"};
			for(int i=0;i<200;++i){
				final String f = images[i%4];
				executor.execute(new Runnable(){
					@Override
					public void run() {
						try {
							Map<CodeInfo, Double> result = facedb.detectAndCompareFaces(featureBean.getMd5(), 
									FacedbTest.class.getResourceAsStream(f), 0).asMap();
							int count = 0;
							for( Entry<CodeInfo, Double> entry:result.entrySet()){
								log("entry {} simility: {}  {}",++count,entry.getValue(),entry.getKey().getPos());
							}
						} catch (Exception e) {
							e.printStackTrace();
						} 
					}});
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		} finally{
			executor.shutdown();
			while(!executor.isTerminated()){
				executor.awaitTermination(10, TimeUnit.SECONDS);
			}
			boolean b = facedb.deleteImage(imageMd5, true);
			assertTrue(b);
		}

	}
}
