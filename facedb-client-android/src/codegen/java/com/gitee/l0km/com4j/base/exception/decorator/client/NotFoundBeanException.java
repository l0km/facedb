// ______________________________________________________
// Generated by codegen - https://gitee.com/l0km/codegen 
// template: thrift/client_thrifty/perstruct/exception.decorator.class.vm
// ______________________________________________________
package com.gitee.l0km.com4j.base.exception.decorator.client;
import com.gitee.l0km.xthrift.base.ThriftDecorator;
import com.gitee.l0km.xthrift.base.exception.BaseServiceException;

import java.io.PrintStream;
import java.io.PrintWriter;
import com.facebook.swift.codec.ThriftStruct;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * decorator pattern 装饰者模式代理{@link com.gitee.l0km.com4j.base.exception.NotFoundBeanException}<br>
 * 代理对象必须有默认构造方法,String参数构造方法可选,必须是普通类型,不可以有@ThriftStruct注释<br>
 * 转发所有{@link com.gitee.l0km.com4j.base.exception.NotFoundBeanException}get/set方法到{@link #delegate()}指定的实例,<br>
 * 计算机生成代码(generated by automated tools ThriftServiceDecoratorGenerator @author guyadong)<br>
 * @author guyadong
 *
 */
@ThriftStruct
public final class NotFoundBeanException extends BaseServiceException 
    implements ThriftDecorator<com.gitee.l0km.com4j.base.exception.NotFoundBeanException> {
    private static final long serialVersionUID = 1L;
    private final com.gitee.l0km.com4j.base.exception.NotFoundBeanException delegate;

    public NotFoundBeanException(){
        this(new com.gitee.l0km.com4j.base.exception.NotFoundBeanException());
    }
    public NotFoundBeanException(String message){
        this(new com.gitee.l0km.com4j.base.exception.NotFoundBeanException(message));
    }
    public NotFoundBeanException(com.gitee.l0km.com4j.base.exception.NotFoundBeanException delegate) {
        super(checkNotNull(delegate,"delegate is null").getMessage(),delegate.getCause());
        // 检查被装饰类是否有@ThriftStruct注释
        if(delegate.getClass().isAnnotationPresent(ThriftStruct.class)){
            throw new IllegalArgumentException(
                String.format("NOT ALLOW %s have @ThriftStruct annotation",delegate.getClass().getName()));
        }
        this.delegate = delegate;
    }
    /**
     * 返回被装饰的{@link com.gitee.l0km.com4j.base.exception.NotFoundBeanException}实例
     */
    @Override
    public com.gitee.l0km.com4j.base.exception.NotFoundBeanException delegate() {
        return delegate;
    }
    @Override
    public void printStackTrace() {
        delegate().printStackTrace();
    }
    @Override
    public void printStackTrace(PrintStream s) {
        delegate().printStackTrace(s);
    }
    @Override
    public void printStackTrace(PrintWriter s) {
        delegate().printStackTrace(s);
    }
    @Override
    public StackTraceElement[] getStackTrace() {
        return delegate().getStackTrace();
    }
    @Override
    public Throwable initCause(Throwable cause) {
        return delegate().initCause(cause);
    }
    @Override
    public int hashCode() {
        return delegate().hashCode();
    }
    @Override
    public boolean equals(Object obj) {
        return delegate().equals(obj);
    }
    @Override
    public String toString() {
        return delegate().toString();
    }
}