package net.gdface.facedb;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.gitee.l0km.com4j.base.exception.NotFoundBeanException;
import com.gitee.l0km.ximage.ImageErrorException;
import com.gitee.l0km.ximage.MatType;

import net.gdface.facedb.db.FaceBean;
import net.gdface.facedb.db.FeatureBean;
import net.gdface.facedb.db.ImageBean;
import net.gdface.sdk.CodeInfo;
import net.gdface.sdk.CompareResult;
import net.gdface.sdk.NotFaceDetectedException;

/**
 * 只是用于代码生成，提供一个完整的接口实现类
 * @author guyadong
 *
 */
class FacedbDefaultImpl implements FaceDb {

	public FacedbDefaultImpl() {
		super();
	}

	@Override
	public CodeInfo[] detectAndGetCodeInfo(byte[] imgData) throws ImageErrorException {
		
		return null;
	}

	@Override
	public CodeInfo[] detectAndGetCodeInfo(MatType matType, byte[] matData, int width, int height) throws ImageErrorException {
		return null;
	}
	
	@Override
	public FeatureBean addFeature(byte[] feature, Map<ByteBuffer, CodeInfo> faces) throws DuplicateRecordException {
		
		return null;
	}

	@Override
	public ImageBean addImage(byte[] imgData, List<CodeInfo> features) throws DuplicateRecordException {
		
		return null;
	}

	@Override
	public ImageBean addImageIfAbsent(byte[] imgData, CodeInfo code, double similarty)
			throws NotFaceDetectedException, ImageErrorException {
		
		return null;
	}

	@Override
	public double compareFeature(String featureId, byte[] feature) throws NotFoundBeanException {
		
		return 0;
	}

	@Override
	public double compareFeatureId(String featureId1, String featureId2) throws NotFoundBeanException {
		
		return 0;
	}

	@Override
	public double[] compareFeatures(String featureId, CodeInfo[] features) throws NotFoundBeanException {
		
		return null;
	}

	@Override
	public double[] compareFaces(String featureId, byte[] imgData, CodeInfo[] facePos)
			throws NotFoundBeanException, NotFaceDetectedException {
		
		return null;
	}

	@Override
	public boolean deleteImage(String imgMd5, boolean cascade) {
		
		return false;
	}

	@Override
	public int deleteImages(List<String> imgMd5List, boolean cascade) {
		
		return 0;
	}

	@Override
	public boolean deleteFeature(String featureId, boolean cascade) {
		
		return false;
	}

	@Override
	public int deleteFeatures(List<String> featureIdList, boolean cascade) {
		
		return 0;
	}

	@Override
	public ImageBean detectAndAddFeatures(byte[] imgData, int faceNum)
			throws DuplicateRecordException, ImageErrorException, NotFaceDetectedException {
		
		return null;
	}

	@Override
	public CompareResult detectAndCompareFaces(String featureId, byte[] imgData, int faceNum)
			throws NotFoundBeanException, ImageErrorException, NotFaceDetectedException {
		
		return null;
	}

	@Override
	public SearchResult[] detectAndSearchFaces(byte[] imgData, double similarty, int rows, String where)
			throws ImageErrorException, NotFaceDetectedException {
		
		return null;
	}

	@Override
	public FeatureBean getFeatureByFaceId(int faceId) {
		
		return null;
	}

	@Override
	public FeatureBean getFeature(String featureId) {
		
		return null;
	}

	@Override
	public List<FeatureBean> getFeaturesByImageMd5(String imageMd5) {
		
		return null;
	}

	@Override
	public FeatureBean getFeatureByImageMd5(String imageMd5) {
		
		return null;
	}

	@Override
	public FaceBean getFaceByFeatureId(String featureId) {
		
		return null;
	}

	@Override
	public List<FaceBean> getFacesByImageMd5(String imageMd5) {
		
		return null;
	}

	@Override
	public FaceBean getFaceByImageMd5(String imageMd5) {
		
		return null;
	}

	@Override
	public List<CodeInfo> getCodeInfosByFeatureId(String featureId) {
		
		return null;
	}

	@Override
	public CodeInfo getCodeInfoByFeatureId(String featureId) {
		
		return null;
	}

	@Override
	public CodeInfo getCodeInfo(int faceId) {
		
		return null;
	}

	@Override
	public List<CodeInfo> getCodeInfosByImageMd5(String imageMd5) {
		
		return null;
	}

	@Override
	public CodeInfo getCodeInfoByImageMd5(String imageMd5) {
		
		return null;
	}

	@Override
	public ImageBean getImage(String imageMd5) {
		
		return null;
	}

	@Override
	public ImageBean getImage(String primaryKey, String refType) {
		
		return null;
	}

	@Override
	public ImageBean getImageByFaceId(int faceId) {
		
		return null;
	}

	@Override
	public byte[] getImageBytes(String imageMd5) {
		
		return null;
	}

	@Override
	public ImageBean getImageByFeatureId(String featureId) {
		
		return null;
	}

	@Override
	public List<ImageBean> getImagesByFeatureId(String featureId) {
		
		return null;
	}

	@Override
	public FaceBean getFace(int faceId) {
		
		return null;
	}

	@Override
	public List<FaceBean> getFacesByFeatureId(String featureId) {
		
		return null;
	}

	@Override
	public boolean hasFeature(byte[] feature) {
		
		return false;
	}

	@Override
	public boolean hasFeatureByMD5(String featureId) {
		
		return false;
	}

	@Override
	public boolean hasImage(String imageMd5) {
		
		return false;
	}

	@Override
	public SearchResult[] searchFaces(byte[] imgData, CodeInfo facePos, double similarty, int rows, String where)
			throws NotFaceDetectedException, ImageErrorException {
		
		return null;
	}
	@Override
	public SearchResult[] searchFaces(MatType matType, byte[] matData, int width, int height, CodeInfo facePos, double similarty,
			int rows, String where) throws NotFaceDetectedException {
		return null;
	}

	@Override
	public SearchResult[] searchFeatures(byte[] feature, double similarty, int rows, String where) {
		
		return null;
	}

	@Override
	public int getImageCount(String where) {
		
		return 0;
	}

	@Override
	public int getFaceCount(String where) {
		
		return 0;
	}

	@Override
	public int getFeatureCount() {
		
		return 0;
	}

	@Override
	public List<String> loadImagesMd5ByWhere(String where) {
		
		return null;
	}

	@Override
	public List<String> loadFeaturesMd5ByCreateTime(Date timestamp) {
		
		return null;
	}

	@Override
	public List<String> loadImagesMd5ByCreateTime(Date timestamp) {
		
		return null;
	}

	@Override
	public List<String> loadFeaturesMd5ByWhere(String where) {
		
		return null;
	}

	@Override
	public List<ImageBean> loadImagesByWhere(String where, int startRow, int numRows) {
		
		return null;
	}

	@Override
	public boolean isLocal() {
		
		return false;
	}

	@Override
	public Map<String, String> dbCapacity() {
		
		return null;
	}

	public byte[] getImageBytes(String primaryKey, String refType) {
		return null;
	}

	public List<String> loadFeaturesMd5ByCreateTime(String timestamp) {
		return null;
	}

	public List<String> loadImagesMd5ByCreateTime(String timestamp) {
		return null;
	}

}
