package net.gdface.facedb;

import static com.gitee.l0km.com4j.base.BinaryUtils.hex2Bytes;
import static com.gitee.l0km.com4j.base.BinaryUtils.toHex;

import net.gdface.sdk.FseResult;
import net.gdface.sdk.fse.CodeBean;

public class SearchResult extends FseResult {

	public SearchResult() {
	}
	public SearchResult(byte[] featureId, String owner, double similarity) {
		super(featureId, owner, similarity);
	}

	/**
	 * 返回特征码ID(32字符HEX字符串)
	 * @return 特征码ID 
	 */
	public String getHexFeatureId() {
		return toHex(getFeatureId());
	}
	/**
	 * @param hexFeatureId 特征码ID(32字符HEX字符串)
	 */
	public void setHexFeatureId(String hexFeatureId) {
		setFeatureId(hex2Bytes(hexFeatureId));
	}
	public static final SearchResult[] toSearchResult(CodeBean[] fseResults){
		if(fseResults != null){
			SearchResult[] result = new SearchResult[fseResults.length];
			for(int i =0;i<result.length;++i){
				result[i] = new SearchResult(fseResults[i].id,fseResults[i].imgMD5,fseResults[i].similarity);
			}
			return result;
		}
		return null;
	}
}
