package net.gdface.facedb;

/**
 * FaceDb 能力描述字段名常量定义
 * @author guyadong
 *
 */
public interface CapcityFieldConstant {
	/**
	 * 是否支持人脸特征搜索(1:N对比)<br>
	 */
	public static String C_SUPPORT_SEARCH = "SUPPORT_SEARCH";
	/**
	 * 是否有人脸识别算法(FaceApi实例)支持<br>
	 */
	public static String C_FACEAPI_ENABLE = "FACEAPI_ENABLE";
}
