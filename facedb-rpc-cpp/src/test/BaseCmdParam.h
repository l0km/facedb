#ifndef WRAPPER_BaseCmdParam_H
#define WRAPPER_BaseCmdParam_H
#include "file_utilits.h"
#include "cmdline.h"

namespace gdface {
	// client运行参数配置基类
	class BaseClientConfig {
	public:
		const int FACEDB_DEFAULT_PORT = 26431;
		const std::string DEFAULT_HOST = "localhost";
		const std::string PARAM_HOST_NAME = "host";
		const std::string PARAM_PORT_NAME = "port";
		cmdline::parser cmdparam;
		std::string host;
		int port;
		inline BaseClientConfig() {
			cmdparam.add<std::string>(PARAM_HOST_NAME, 'h', "host name", false, DEFAULT_HOST);
			cmdparam.add<int>(PARAM_PORT_NAME, 'p', "port number", false, FACEDB_DEFAULT_PORT, cmdline::range(1, 65535));
		}
		inline void parse(int argc, char*argv[]) {
			auto program_name = get_file_name(argv[0]);
			cmdparam.set_program_name(program_name);
			cmdparam.parse_check(argc, argv);
			afterParse();
		}
	protected:
		inline virtual void afterParse() {
			host = cmdparam.get<std::string>(PARAM_HOST_NAME);
			port = cmdparam.get<int>(PARAM_PORT_NAME);
		}
	};
} /* namespace gdface */
#endif // !WRAPPER_BaseCmdParam_H


