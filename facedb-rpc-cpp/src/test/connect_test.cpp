/*
* connect_test.cpp
* client端连接测试
*  Created on: 2018年05月13日
*      Author: guyadong
*/
#include <iostream>
#include <facedbclient/FaceDbIfClient.h>
#include "BaseCmdParam.h"
#include "cast_utilits.h"
#include <string>
#include <algorithm>
#include "sample_log.h"

using namespace gdface;
using namespace net::gdface::utils;
using namespace net::gdface::facedb;

template<typename Map>
void print_map(Map& m)
{
	std::cout << '{';
	for (auto& p : m)
		std::cout << p.first << ':' << p.second << ' ';
	std::cout << "}\n";
}
void testSearch() {
	auto client = FaceDbIfClient("127.0.0.1", 26431);
	try {
		// 加载图像数据为std::string
		std::string imgData;
			// 调用人脸搜索方法
			auto codes = client.detectAndSearchFaces(imgData, 0.5, 1, nullptr);
		if (codes->size()>0) {
			auto e=(*codes)[0];
			std::cout << (*codes)[0] << std::endl;
		}
	}
	catch (net::gdface::image::ImageErrorException &e)
	{
		// 捕获服务端抛出的图像数据异常
		std::cout << e.serviceStackTraceMessage << std::endl;
	}
	catch (net::gdface::sdk::NotFaceDetectedException &e)
	{
		// 捕获服务端抛出的NotFaceDetectedException异常
		std::cout << e.serviceStackTraceMessage << std::endl;
	}
	catch (net::gdface::thrift::exception::ServiceRuntimeException &e)
	{
		// 捕获服务端抛出的运行时异常(RuntimeException)
		std::cout << e.serviceStackTraceMessage << std::endl;
	}
	catch (std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}
int main(int argc, char *argv[]) {
	BaseClientConfig param;
	param.parse(argc, argv);
	auto client = FaceDbIfClient(param.host, param.port);
	try {
		SAMPLE_OUT("connect service:{}@{} ...", param.port, param.host);
		// 调用 isLocal()接口方法
		SAMPLE_OUT("isLocal respone:{}", client.isLocal());
		// 调用 multiFaceFeature()接口方法
		SAMPLE_OUT("multiFaceFeature respone:{}", client.dbCapacity().get());

	}
	catch (std::exception& tx) {
		SAMPLE_ERR("ERROR: {}", tx.what());
	}
}