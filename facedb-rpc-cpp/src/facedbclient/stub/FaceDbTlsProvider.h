// ______________________________________________________
// Generated by codegen - https://gitee.com/l0km/codegen 
// template: thrift_cpp/client/perservice/tls_provider.h.vm
// ______________________________________________________ 
#ifndef _FACE_DB_TLS_PROVIDER_H
#define _FACE_DB_TLS_PROVIDER_H
/**
 * 计算机生成代码(generated by automated tools ThriftServiceDecoratorGenerator @author guyadong)<br>
 * template: thrift_cpp/client/perservice/tls_provider.h.vm <br>
 * @author guyadong
 *
 */
#include "FaceDbClientProviderIf.h"
namespace net{namespace gdface{namespace facedb{
/**
 * 基于线程局部变量(thread local storage)实现 net::gdface::facedb::FaceDbClientProviderIf 接口
 */
class FaceDbTlsProvider : virtual public FaceDbClientProviderIf {
private:
  std::string host;
  int port;
  /* Connect timeout in mills */
  int connTimeout;
  /* Send timeout in mills */
  int sendTimeout;
  /* Receive timeout in mills */
  int recvTimeout;
  void makeClient() const;

public:
  // constructor
  // 根据主机和端口号创建实例
  FaceDbTlsProvider(const std::string & host, int port);

  // deconstructor
  virtual ~FaceDbTlsProvider();

  // 重新指定主机和端口号
  virtual void resetClient(const std::string & host, int port);

  // 设置超时参数
  // @param connTimeout Connect timeout in mills,ignore if less than or equal to 0
  // @param sendTimeout Send timeout in mills,ignore if less than or equal to 0
  // @param recvTimeout Receive timeout in mills,ignore if less than or equal to 0
  virtual void setTimeout(int connTimeout, int sendTimeout, int recvTimeout);

  virtual ::gdface::FaceDbClient & get() const;

  virtual std::string getHost() const noexcept;

  virtual int getPort() const noexcept;

  virtual ::apache::thrift::transport::TTransport & getTransport() const ;

}; /** class FaceDbTlsProvider */ 
} /* namespace facedb */} /* namespace gdface */} /* namespace net */
#endif /** _FACE_DB_TLS_PROVIDER_H */