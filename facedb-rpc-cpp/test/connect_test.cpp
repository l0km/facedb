/*
* connect_test.cpp
* client端连接测试
*  Created on: 2018年05月13日
*      Author: guyadong
*/
#include <iostream>
#include <facedbclient/FaceDbIfClient.h>
#include "BaseCmdParam.h"
#include "cast_utilits.h"
#include <string>
#include <algorithm>
#include "sample_log.h"

using namespace gdface;
using namespace net::gdface::utils;
using namespace net::gdface::facedb;

template<typename Map>
void print_map(Map& m)
{
	std::cout << '{';
	for (auto& p : m)
		std::cout << p.first << ':' << p.second << ' ';
	std::cout << "}\n";
}
int main(int argc, char *argv[]) {
	BaseClientConfig param;
	param.parse(argc, argv);
	auto client = FaceDbIfClient(param.host, param.port);
	try {
		SAMPLE_OUT("connect service:{}@{} ...", param.port, param.host);
		// 调用 isLocal()接口方法
		SAMPLE_OUT("isLocal respone:{}", client.isLocal());
		// 调用 multiFaceFeature()接口方法
		SAMPLE_OUT("multiFaceFeature respone:{}", client.dbCapacity().get());

	}
	catch (std::exception& tx) {
		SAMPLE_ERR("ERROR: {}", tx.what());
	}
}