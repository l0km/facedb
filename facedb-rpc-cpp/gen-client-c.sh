#!/bin/bash
# 生成 FaceDb cpp client代码脚本
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
OUT_FOLDER=$sh_folder/src/facedbclient_c/stub
# 指定thrift compiler位置
[ $(which thrift) >/dev/null ] && THRIFT_EXE=thrift
$THRIFT_EXE --version || exit

if [ -e "$OUT_FOLDER" ] ;
then
	rm  "$OUT_FOLDER"/FaceDb*.c* 1>/dev/null 2>/dev/null
	rm  "$OUT_FOLDER"/FaceDb*.h 1>/dev/null 2>/dev/null
else
	mkdir -p "$OUT_FOLDER"
fi

$THRIFT_EXE --gen c_glib \
	-out "$OUT_FOLDER" \
	"$sh_folder/../facedb-service/FaceDb.thrift" || exit

popd
