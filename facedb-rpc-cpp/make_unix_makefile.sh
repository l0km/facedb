#!/bin/bash
# facedbclient 编译脚本
# Optional Environment Variables: 
# 		 MACHINE 目标平台, such as x86_64-linux-gnu,默认使用当前系统平台
#        PREFIX 安装路径
#        PROJECT_FOLDER cmake 生成的工程文件(Makefile)文件夹
#        BUILD_TYPE 编译版本类型(DEBUG|RELEASE),默认 DEBUG

sh_folder=$(dirname $(readlink -f $0))
folder_name=$(basename $sh_folder) 
# 定义编译的版本类型(DEBUG|RELEASE)
build_type=Debug
[[ "${BUILD_TYPE^^}" =~ DEBUG|RELEASE ]] && build_type=${BUILD_TYPE,,} && build_type=${build_type^}

echo build_type=$build_type
# 是否编译测试程序
build_test=True

machine=$(g++ -dumpmachine)

[ -n "$MACHINE" ] && machine=$MACHINE

[ -n "$PREFIX" ] && install_prefix="$PREFIX"
[ -z "$PREFIX" ] && install_prefix=$sh_folder/release/facedbclient_$machine
[ -e "$install_prefix" ] && rm -fr "$install_prefix"

[ -n "$PROJECT_FOLDER" ] && prj_folder="$PROJECT_FOLDER"
[ -z "$PROJECT_FOLDER" ] && prj_folder=$sh_folder/../$folder_name.gnu

pushd $sh_folder/..

[ -d $prj_folder ] && rm -fr $prj_folder
mkdir -p $prj_folder || exit

pushd $prj_folder

echo "creating x86_64 Project for gnu ..."
conan install $sh_folder -of . --build missing -s build_type=$build_type || exit
cmake "$sh_folder" -G "Eclipse CDT4 - Unix Makefiles" \
	-DCMAKE_BUILD_TYPE=$build_type \
	-DBUILD_TEST=$build_test \
	-DCMAKE_TOOLCHAIN_FILE=./conan_toolchain.cmake \
	-DCMAKE_INSTALL_PREFIX=$install_prefix || exit 

cmake --build   . --config $build_type || exit 
cmake --install . --config $build_type || exit 

popd
popd

