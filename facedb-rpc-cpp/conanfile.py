from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain
from conan.tools.env import VirtualBuildEnv
import xml.etree.ElementTree as ET
import os

class FacedbConan(ConanFile):
    name = "facedbclient"
    # Optional metadata
    license = "BSD-2-Clause"
    author = "guyadong 10km0811@sohu.com"
    url = "https://gitee.com/l0km/facedb"
    description = "Facial data management"
    topics = ("facedb","facial")

    requires = "thrift/0.13.0"
    tool_requires = "cmake/[>=3.15.7]"
    package_type = "library"
    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False],
        # 是否编译测试代码
        "build_test": [True, False]}
    default_options = {"shared": False, "fPIC": True, "build_test": False}
    # 将包含版本号的pom.xml与conanfile.py放在一起
    exports = "pom.xml"
    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt","src/CMakeLists.txt", "src/facedbclient/*","src/facedbclient/CMakeLists.txt","src/gfaux_tools/*","src/gfaux_tools/CMakeLists.txt","src/test/*","src/test/CMakeLists.txt", "cmake/*.cmake.in"

    def set_version(self):
        # 从pom.xml中解析 parent/version 节点获取pom.xml中定义的版本
        root = ET.parse(os.path.join(self.recipe_folder,"pom.xml")).getroot()
        self.version = root.find("{*}parent/{*}version").text.lower()
        self.output.info(f"Read version from pom.xml:{self.version}")

    def config_options(self):
        # 编译动态库时不生成测试和exe
        if self.options.shared :
            self.options.build_test = False

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def requirements(self):
        self.requires("common_source_cpp/1.0.1", transitive_headers=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["BUILD_TEST"] = self.options.build_test
        tc.generate()

        cd = CMakeDeps(self)
        cd.generate()

        env = VirtualBuildEnv(self)
        env.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_id(self):
        del self.info.options.build_test

    def package_info(self):
        # self.cpp_info.libs = ["facedbclient"]
        # 指定将安装路径加入CMAKE_PREFIX_PATH,以便执行find_package时能扫描到安装路径
        self.cpp_info.builddirs.append(self.package_folder)
        # 抑制conan生成akcore-config.cmake使用akcore自己生成的cmake配置文件 
        self.cpp_info.set_property("cmake_find_mode", "none")
        # cmake_find_mode为none时不需要配置此参数
        # self.cpp_info.requires=["thrift::thrift","boost::filesystem","common_source_cpp::common_source_cpp"]
