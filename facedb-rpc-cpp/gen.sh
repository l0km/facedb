#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
./gen-client-c.sh || exit
./gen-client-cpp.sh || exit
./gen-decorator-client-cpp.sh || exit
popd
