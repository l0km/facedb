echo off
rem 生成 FaceDb cpp client代码脚本
set sh_folder=%~dp0
rem 删除最后的 '\'
set sh_folder=%sh_folder:~0,-1%
pushd "%sh_folder%"
set OUT_FOLDER=%sh_folder%\src\facedbclient_c\stub
:: 指定thrift compiler位置
where thrift >nul 2>nul
if %ERRORLEVEL% == 0 (
	set THRIFT_EXE=thrift
	goto :gen
	)
if not exist "%THRIFT_EXE%" (
	echo "not found thrift.exe,please build thrift"
	exit /B -1
	)
	
:gen
if exist "%OUT_FOLDER%" (
	del  "%OUT_FOLDER%"\FaceDb*.c* >nul 2>nul
	del  "%OUT_FOLDER%"\FaceDb*.h* >nul 2>nul
	)
if not exist "%OUT_FOLDER%" mkdir  "%OUT_FOLDER%"

%THRIFT_EXE% --gen c_glib ^
	-out "%OUT_FOLDER%" ^
	%sh_folder%\..\facedb-service\FaceDb.thrift 

popd
