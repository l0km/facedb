package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("CompareResult")
public final class CompareResult
{
    public CompareResult() {
    }

    private List<CodeInfo> codes;

    @ThriftField(value=1, name="codes", requiredness=Requiredness.OPTIONAL)
    public List<CodeInfo> getCodes() { return codes; }

    @ThriftField
    public void setCodes(final List<CodeInfo> codes) { this.codes = codes; }

    private List<Double> similartys;

    @ThriftField(value=2, name="similartys", requiredness=Requiredness.OPTIONAL)
    public List<Double> getSimilartys() { return similartys; }

    @ThriftField
    public void setSimilartys(final List<Double> similartys) { this.similartys = similartys; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("codes", codes)
            .add("similartys", similartys)
            .toString();
    }
}
