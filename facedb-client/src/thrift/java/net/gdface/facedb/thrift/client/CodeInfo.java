package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("CodeInfo")
public final class CodeInfo
{
    public CodeInfo() {
    }

    private FAngle angle;

    @ThriftField(value=1, name="angle", requiredness=Requiredness.OPTIONAL)
    public FAngle getAngle() { return angle; }

    @ThriftField
    public void setAngle(final FAngle angle) { this.angle = angle; }

    private byte [] code;

    @ThriftField(value=2, name="code", requiredness=Requiredness.OPTIONAL)
    public byte [] getCode() { return code; }

    @ThriftField
    public void setCode(final byte [] code) { this.code = code; }

    private EyeInfo ei;

    @ThriftField(value=3, name="ei", requiredness=Requiredness.OPTIONAL)
    public EyeInfo getEi() { return ei; }

    @ThriftField
    public void setEi(final EyeInfo ei) { this.ei = ei; }

    private byte [] facialData;

    @ThriftField(value=4, name="facialData", requiredness=Requiredness.OPTIONAL)
    public byte [] getFacialData() { return facialData; }

    @ThriftField
    public void setFacialData(final byte [] facialData) { this.facialData = facialData; }

    private FInt2 mouth;

    @ThriftField(value=5, name="mouth", requiredness=Requiredness.OPTIONAL)
    public FInt2 getMouth() { return mouth; }

    @ThriftField
    public void setMouth(final FInt2 mouth) { this.mouth = mouth; }

    private FInt2 nose;

    @ThriftField(value=6, name="nose", requiredness=Requiredness.OPTIONAL)
    public FInt2 getNose() { return nose; }

    @ThriftField
    public void setNose(final FInt2 nose) { this.nose = nose; }

    private FInt2 offset;

    @ThriftField(value=7, name="offset", requiredness=Requiredness.OPTIONAL)
    public FInt2 getOffset() { return offset; }

    @ThriftField
    public void setOffset(final FInt2 offset) { this.offset = offset; }

    private FRect pos;

    @ThriftField(value=8, name="pos", requiredness=Requiredness.OPTIONAL)
    public FRect getPos() { return pos; }

    @ThriftField
    public void setPos(final FRect pos) { this.pos = pos; }

    private int rotate;

    @ThriftField(value=9, name="rotate", requiredness=Requiredness.REQUIRED)
    public int getRotate() { return rotate; }

    @ThriftField
    public void setRotate(final int rotate) { this.rotate = rotate; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("angle", angle)
            .add("code", code)
            .add("ei", ei)
            .add("facialData", facialData)
            .add("mouth", mouth)
            .add("nose", nose)
            .add("offset", offset)
            .add("pos", pos)
            .add("rotate", rotate)
            .toString();
    }
}
