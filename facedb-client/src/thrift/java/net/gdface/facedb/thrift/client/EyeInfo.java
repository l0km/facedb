package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("EyeInfo")
public final class EyeInfo
{
    public EyeInfo() {
    }

    private int leftx;

    @ThriftField(value=1, name="leftx", requiredness=Requiredness.REQUIRED)
    public int getLeftx() { return leftx; }

    @ThriftField
    public void setLeftx(final int leftx) { this.leftx = leftx; }

    private int lefty;

    @ThriftField(value=2, name="lefty", requiredness=Requiredness.REQUIRED)
    public int getLefty() { return lefty; }

    @ThriftField
    public void setLefty(final int lefty) { this.lefty = lefty; }

    private int rightx;

    @ThriftField(value=3, name="rightx", requiredness=Requiredness.REQUIRED)
    public int getRightx() { return rightx; }

    @ThriftField
    public void setRightx(final int rightx) { this.rightx = rightx; }

    private int righty;

    @ThriftField(value=4, name="righty", requiredness=Requiredness.REQUIRED)
    public int getRighty() { return righty; }

    @ThriftField
    public void setRighty(final int righty) { this.righty = righty; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("leftx", leftx)
            .add("lefty", lefty)
            .add("rightx", rightx)
            .add("righty", righty)
            .toString();
    }
}
