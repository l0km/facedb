package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;

public enum MatType
{
    NV21(0), RGB(1), BGR(2), RGBA(3), GRAY(4);

    private final int value;

    MatType(int value)
    {
        this.value = value;
    }

    @ThriftEnumValue
    public int getValue()
    {
        return value;
    }
}
