package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("SearchResult")
public final class SearchResult
{
    public SearchResult() {
    }

    private byte [] featureId;

    @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL)
    public byte [] getFeatureId() { return featureId; }

    @ThriftField
    public void setFeatureId(final byte [] featureId) { this.featureId = featureId; }

    private String hexFeatureId;

    @ThriftField(value=2, name="hexFeatureId", requiredness=Requiredness.OPTIONAL)
    public String getHexFeatureId() { return hexFeatureId; }

    @ThriftField
    public void setHexFeatureId(final String hexFeatureId) { this.hexFeatureId = hexFeatureId; }

    private String owner;

    @ThriftField(value=3, name="owner", requiredness=Requiredness.OPTIONAL)
    public String getOwner() { return owner; }

    @ThriftField
    public void setOwner(final String owner) { this.owner = owner; }

    private double similarity;

    @ThriftField(value=4, name="similarity", requiredness=Requiredness.REQUIRED)
    public double getSimilarity() { return similarity; }

    @ThriftField
    public void setSimilarity(final double similarity) { this.similarity = similarity; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("featureId", featureId)
            .add("hexFeatureId", hexFeatureId)
            .add("owner", owner)
            .add("similarity", similarity)
            .toString();
    }
}
