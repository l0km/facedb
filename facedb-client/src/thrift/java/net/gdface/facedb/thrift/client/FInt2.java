package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("FInt2")
public final class FInt2
{
    public FInt2() {
    }

    private int x;

    @ThriftField(value=1, name="x", requiredness=Requiredness.REQUIRED)
    public int getX() { return x; }

    @ThriftField
    public void setX(final int x) { this.x = x; }

    private int y;

    @ThriftField(value=2, name="y", requiredness=Requiredness.REQUIRED)
    public int getY() { return y; }

    @ThriftField
    public void setY(final int y) { this.y = y; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("x", x)
            .add("y", y)
            .toString();
    }
}
