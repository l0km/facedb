package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import com.facebook.swift.service.*;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.*;
import java.util.*;

@ThriftService("FaceDb")
public interface FaceDb
{
    @ThriftService("FaceDb")
    public interface Async
    {
        @ThriftMethod(value = "addFeature",
                      exception = {
                          @ThriftException(type=DuplicateRecordException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<FeatureBean> addFeature(
            @ThriftField(value=1, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature,
            @ThriftField(value=2, name="faces", requiredness=Requiredness.OPTIONAL) final Map<byte [], CodeInfo> faces
        );

        @ThriftMethod(value = "addImage",
                      exception = {
                          @ThriftException(type=DuplicateRecordException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<ImageBean> addImage(
            @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
            @ThriftField(value=2, name="features", requiredness=Requiredness.OPTIONAL) final List<CodeInfo> features
        );

        @ThriftMethod(value = "addImageIfAbsent",
                      exception = {
                          @ThriftException(type=ImageErrorException.class, id=1),
                          @ThriftException(type=NotFaceDetectedException.class, id=2),
                          @ThriftException(type=ServiceRuntimeException.class, id=3)
                      })
        ListenableFuture<ImageBean> addImageIfAbsent(
            @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
            @ThriftField(value=2, name="code", requiredness=Requiredness.OPTIONAL) final CodeInfo code,
            @ThriftField(value=3, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty
        );

        @ThriftMethod(value = "compareFaces",
                      exception = {
                          @ThriftException(type=NotFoundBeanException.class, id=1),
                          @ThriftException(type=NotFaceDetectedException.class, id=2),
                          @ThriftException(type=ServiceRuntimeException.class, id=3)
                      })
        ListenableFuture<List<Double>> compareFaces(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
            @ThriftField(value=2, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
            @ThriftField(value=3, name="facePos", requiredness=Requiredness.OPTIONAL) final List<CodeInfo> facePos
        );

        @ThriftMethod(value = "compareFeature",
                      exception = {
                          @ThriftException(type=NotFoundBeanException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<Double> compareFeature(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
            @ThriftField(value=2, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature
        );

        @ThriftMethod(value = "compareFeatureId",
                      exception = {
                          @ThriftException(type=NotFoundBeanException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<Double> compareFeatureId(
            @ThriftField(value=1, name="featureId1", requiredness=Requiredness.OPTIONAL) final String featureId1,
            @ThriftField(value=2, name="featureId2", requiredness=Requiredness.OPTIONAL) final String featureId2
        );

        @ThriftMethod(value = "compareFeatures",
                      exception = {
                          @ThriftException(type=NotFoundBeanException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<List<Double>> compareFeatures(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
            @ThriftField(value=2, name="features", requiredness=Requiredness.OPTIONAL) final List<CodeInfo> features
        );

        @ThriftMethod(value = "dbCapacity",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Map<String, String>> dbCapacity();

        @ThriftMethod(value = "deleteFeature",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Boolean> deleteFeature(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
            @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
        );

        @ThriftMethod(value = "deleteFeatures",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Integer> deleteFeatures(
            @ThriftField(value=1, name="featureIdList", requiredness=Requiredness.OPTIONAL) final List<String> featureIdList,
            @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
        );

        @ThriftMethod(value = "deleteImage",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Boolean> deleteImage(
            @ThriftField(value=1, name="imgMd5", requiredness=Requiredness.OPTIONAL) final String imgMd5,
            @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
        );

        @ThriftMethod(value = "deleteImages",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Integer> deleteImages(
            @ThriftField(value=1, name="imgMd5List", requiredness=Requiredness.OPTIONAL) final List<String> imgMd5List,
            @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
        );

        @ThriftMethod(value = "detectAndAddFeatures",
                      exception = {
                          @ThriftException(type=ImageErrorException.class, id=1),
                          @ThriftException(type=DuplicateRecordException.class, id=2),
                          @ThriftException(type=NotFaceDetectedException.class, id=3),
                          @ThriftException(type=ServiceRuntimeException.class, id=4)
                      })
        ListenableFuture<ImageBean> detectAndAddFeatures(
            @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
            @ThriftField(value=2, name="faceNum", requiredness=Requiredness.REQUIRED) final int faceNum
        );

        @ThriftMethod(value = "detectAndCompareFaces",
                      exception = {
                          @ThriftException(type=NotFoundBeanException.class, id=1),
                          @ThriftException(type=ImageErrorException.class, id=2),
                          @ThriftException(type=NotFaceDetectedException.class, id=3),
                          @ThriftException(type=ServiceRuntimeException.class, id=4)
                      })
        ListenableFuture<CompareResult> detectAndCompareFaces(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
            @ThriftField(value=2, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
            @ThriftField(value=3, name="faceNum", requiredness=Requiredness.REQUIRED) final int faceNum
        );

        @ThriftMethod(value = "detectAndGetCodeInfo",
                      exception = {
                          @ThriftException(type=ImageErrorException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<List<CodeInfo>> detectAndGetCodeInfo(
            @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData
        );

        @ThriftMethod(value = "detectAndGetCodeInfoMat",
                      exception = {
                          @ThriftException(type=ImageErrorException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<List<CodeInfo>> detectAndGetCodeInfoMat(
            @ThriftField(value=1, name="matType", requiredness=Requiredness.OPTIONAL) final MatType matType,
            @ThriftField(value=2, name="matData", requiredness=Requiredness.OPTIONAL) final byte [] matData,
            @ThriftField(value=3, name="width", requiredness=Requiredness.REQUIRED) final int width,
            @ThriftField(value=4, name="height", requiredness=Requiredness.REQUIRED) final int height
        );

        @ThriftMethod(value = "detectAndSearchFaces",
                      exception = {
                          @ThriftException(type=ImageErrorException.class, id=1),
                          @ThriftException(type=NotFaceDetectedException.class, id=2),
                          @ThriftException(type=ServiceRuntimeException.class, id=3)
                      })
        ListenableFuture<List<SearchResult>> detectAndSearchFaces(
            @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
            @ThriftField(value=2, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
            @ThriftField(value=3, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
            @ThriftField(value=4, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );

        @ThriftMethod(value = "getCodeInfo",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<CodeInfo> getCodeInfo(
            @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
        );

        @ThriftMethod(value = "getCodeInfoByFeatureId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<CodeInfo> getCodeInfoByFeatureId(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "getCodeInfoByImageMd5",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<CodeInfo> getCodeInfoByImageMd5(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getCodeInfosByFeatureId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<CodeInfo>> getCodeInfosByFeatureId(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "getCodeInfosByImageMd5",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<CodeInfo>> getCodeInfosByImageMd5(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getFace",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<FaceBean> getFace(
            @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
        );

        @ThriftMethod(value = "getFaceByFeatureId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<FaceBean> getFaceByFeatureId(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "getFaceByImageMd5",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<FaceBean> getFaceByImageMd5(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getFaceCount",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Integer> getFaceCount(
            @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );

        @ThriftMethod(value = "getFacesByFeatureId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<FaceBean>> getFacesByFeatureId(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "getFacesByImageMd5",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<FaceBean>> getFacesByImageMd5(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getFeature",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<FeatureBean> getFeature(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "getFeatureByFaceId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<FeatureBean> getFeatureByFaceId(
            @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
        );

        @ThriftMethod(value = "getFeatureByImageMd5",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<FeatureBean> getFeatureByImageMd5(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getFeatureCount",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Integer> getFeatureCount();

        @ThriftMethod(value = "getFeaturesByImageMd5",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<FeatureBean>> getFeaturesByImageMd5(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getImage",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<ImageBean> getImage(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getImageByFaceId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<ImageBean> getImageByFaceId(
            @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
        );

        @ThriftMethod(value = "getImageByFeatureId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<ImageBean> getImageByFeatureId(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "getImageBytes",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<byte []> getImageBytes(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "getImageBytesRef",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<byte []> getImageBytesRef(
            @ThriftField(value=1, name="primaryKey", requiredness=Requiredness.OPTIONAL) final String primaryKey,
            @ThriftField(value=2, name="refType", requiredness=Requiredness.OPTIONAL) final String refType
        );

        @ThriftMethod(value = "getImageCount",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Integer> getImageCount(
            @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );

        @ThriftMethod(value = "getImageRef",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<ImageBean> getImageRef(
            @ThriftField(value=1, name="primaryKey", requiredness=Requiredness.OPTIONAL) final String primaryKey,
            @ThriftField(value=2, name="refType", requiredness=Requiredness.OPTIONAL) final String refType
        );

        @ThriftMethod(value = "getImagesByFeatureId",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<ImageBean>> getImagesByFeatureId(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "hasFeature",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Boolean> hasFeature(
            @ThriftField(value=1, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature
        );

        @ThriftMethod(value = "hasFeatureByMD5",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Boolean> hasFeatureByMD5(
            @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
        );

        @ThriftMethod(value = "hasImage",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Boolean> hasImage(
            @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
        );

        @ThriftMethod(value = "isLocal",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<Boolean> isLocal();

        @ThriftMethod(value = "loadFeaturesMd5ByCreateTime",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<String>> loadFeaturesMd5ByCreateTime(
            @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final Long timestamp
        );

        @ThriftMethod(value = "loadFeaturesMd5ByCreateTimeTimeStr",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<String>> loadFeaturesMd5ByCreateTimeTimeStr(
            @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final String timestamp
        );

        @ThriftMethod(value = "loadFeaturesMd5ByWhere",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<String>> loadFeaturesMd5ByWhere(
            @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );

        @ThriftMethod(value = "loadImagesByWhere",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<ImageBean>> loadImagesByWhere(
            @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where,
            @ThriftField(value=2, name="startRow", requiredness=Requiredness.REQUIRED) final int startRow,
            @ThriftField(value=3, name="numRows", requiredness=Requiredness.REQUIRED) final int numRows
        );

        @ThriftMethod(value = "loadImagesMd5ByCreateTime",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<String>> loadImagesMd5ByCreateTime(
            @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final Long timestamp
        );

        @ThriftMethod(value = "loadImagesMd5ByCreateTimeTimeStr",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<String>> loadImagesMd5ByCreateTimeTimeStr(
            @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final String timestamp
        );

        @ThriftMethod(value = "loadImagesMd5ByWhere",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<String>> loadImagesMd5ByWhere(
            @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );

        @ThriftMethod(value = "searchFaces",
                      exception = {
                          @ThriftException(type=ImageErrorException.class, id=1),
                          @ThriftException(type=NotFaceDetectedException.class, id=2),
                          @ThriftException(type=ServiceRuntimeException.class, id=3)
                      })
        ListenableFuture<List<SearchResult>> searchFaces(
            @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
            @ThriftField(value=2, name="facePos", requiredness=Requiredness.OPTIONAL) final CodeInfo facePos,
            @ThriftField(value=3, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
            @ThriftField(value=4, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
            @ThriftField(value=5, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );

        @ThriftMethod(value = "searchFacesMat",
                      exception = {
                          @ThriftException(type=NotFaceDetectedException.class, id=1),
                          @ThriftException(type=ServiceRuntimeException.class, id=2)
                      })
        ListenableFuture<List<SearchResult>> searchFacesMat(
            @ThriftField(value=1, name="matType", requiredness=Requiredness.OPTIONAL) final MatType matType,
            @ThriftField(value=2, name="matData", requiredness=Requiredness.OPTIONAL) final byte [] matData,
            @ThriftField(value=3, name="width", requiredness=Requiredness.REQUIRED) final int width,
            @ThriftField(value=4, name="height", requiredness=Requiredness.REQUIRED) final int height,
            @ThriftField(value=5, name="facePos", requiredness=Requiredness.OPTIONAL) final CodeInfo facePos,
            @ThriftField(value=6, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
            @ThriftField(value=7, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
            @ThriftField(value=8, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );

        @ThriftMethod(value = "searchFeatures",
                      exception = {
                          @ThriftException(type=ServiceRuntimeException.class, id=1)
                      })
        ListenableFuture<List<SearchResult>> searchFeatures(
            @ThriftField(value=1, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature,
            @ThriftField(value=2, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
            @ThriftField(value=3, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
            @ThriftField(value=4, name="where", requiredness=Requiredness.OPTIONAL) final String where
        );
    }
    @ThriftMethod(value = "addFeature",
                  exception = {
                      @ThriftException(type=DuplicateRecordException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    FeatureBean addFeature(
        @ThriftField(value=1, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature,
        @ThriftField(value=2, name="faces", requiredness=Requiredness.OPTIONAL) final Map<byte [], CodeInfo> faces
    ) throws DuplicateRecordException, ServiceRuntimeException;

    @ThriftMethod(value = "addImage",
                  exception = {
                      @ThriftException(type=DuplicateRecordException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    ImageBean addImage(
        @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
        @ThriftField(value=2, name="features", requiredness=Requiredness.OPTIONAL) final List<CodeInfo> features
    ) throws DuplicateRecordException, ServiceRuntimeException;

    @ThriftMethod(value = "addImageIfAbsent",
                  exception = {
                      @ThriftException(type=ImageErrorException.class, id=1),
                      @ThriftException(type=NotFaceDetectedException.class, id=2),
                      @ThriftException(type=ServiceRuntimeException.class, id=3)
                  })
    ImageBean addImageIfAbsent(
        @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
        @ThriftField(value=2, name="code", requiredness=Requiredness.OPTIONAL) final CodeInfo code,
        @ThriftField(value=3, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty
    ) throws ImageErrorException, NotFaceDetectedException, ServiceRuntimeException;

    @ThriftMethod(value = "compareFaces",
                  exception = {
                      @ThriftException(type=NotFoundBeanException.class, id=1),
                      @ThriftException(type=NotFaceDetectedException.class, id=2),
                      @ThriftException(type=ServiceRuntimeException.class, id=3)
                  })
    List<Double> compareFaces(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
        @ThriftField(value=2, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
        @ThriftField(value=3, name="facePos", requiredness=Requiredness.OPTIONAL) final List<CodeInfo> facePos
    ) throws NotFoundBeanException, NotFaceDetectedException, ServiceRuntimeException;

    @ThriftMethod(value = "compareFeature",
                  exception = {
                      @ThriftException(type=NotFoundBeanException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    double compareFeature(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
        @ThriftField(value=2, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature
    ) throws NotFoundBeanException, ServiceRuntimeException;

    @ThriftMethod(value = "compareFeatureId",
                  exception = {
                      @ThriftException(type=NotFoundBeanException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    double compareFeatureId(
        @ThriftField(value=1, name="featureId1", requiredness=Requiredness.OPTIONAL) final String featureId1,
        @ThriftField(value=2, name="featureId2", requiredness=Requiredness.OPTIONAL) final String featureId2
    ) throws NotFoundBeanException, ServiceRuntimeException;

    @ThriftMethod(value = "compareFeatures",
                  exception = {
                      @ThriftException(type=NotFoundBeanException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    List<Double> compareFeatures(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
        @ThriftField(value=2, name="features", requiredness=Requiredness.OPTIONAL) final List<CodeInfo> features
    ) throws NotFoundBeanException, ServiceRuntimeException;

    @ThriftMethod(value = "dbCapacity",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    Map<String, String> dbCapacity() throws ServiceRuntimeException;

    @ThriftMethod(value = "deleteFeature",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    boolean deleteFeature(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
        @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "deleteFeatures",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    int deleteFeatures(
        @ThriftField(value=1, name="featureIdList", requiredness=Requiredness.OPTIONAL) final List<String> featureIdList,
        @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "deleteImage",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    boolean deleteImage(
        @ThriftField(value=1, name="imgMd5", requiredness=Requiredness.OPTIONAL) final String imgMd5,
        @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "deleteImages",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    int deleteImages(
        @ThriftField(value=1, name="imgMd5List", requiredness=Requiredness.OPTIONAL) final List<String> imgMd5List,
        @ThriftField(value=2, name="cascade", requiredness=Requiredness.REQUIRED) final boolean cascade
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "detectAndAddFeatures",
                  exception = {
                      @ThriftException(type=ImageErrorException.class, id=1),
                      @ThriftException(type=DuplicateRecordException.class, id=2),
                      @ThriftException(type=NotFaceDetectedException.class, id=3),
                      @ThriftException(type=ServiceRuntimeException.class, id=4)
                  })
    ImageBean detectAndAddFeatures(
        @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
        @ThriftField(value=2, name="faceNum", requiredness=Requiredness.REQUIRED) final int faceNum
    ) throws ImageErrorException, DuplicateRecordException, NotFaceDetectedException, ServiceRuntimeException;

    @ThriftMethod(value = "detectAndCompareFaces",
                  exception = {
                      @ThriftException(type=NotFoundBeanException.class, id=1),
                      @ThriftException(type=ImageErrorException.class, id=2),
                      @ThriftException(type=NotFaceDetectedException.class, id=3),
                      @ThriftException(type=ServiceRuntimeException.class, id=4)
                  })
    CompareResult detectAndCompareFaces(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId,
        @ThriftField(value=2, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
        @ThriftField(value=3, name="faceNum", requiredness=Requiredness.REQUIRED) final int faceNum
    ) throws NotFoundBeanException, ImageErrorException, NotFaceDetectedException, ServiceRuntimeException;

    @ThriftMethod(value = "detectAndGetCodeInfo",
                  exception = {
                      @ThriftException(type=ImageErrorException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    List<CodeInfo> detectAndGetCodeInfo(
        @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData
    ) throws ImageErrorException, ServiceRuntimeException;

    @ThriftMethod(value = "detectAndGetCodeInfoMat",
                  exception = {
                      @ThriftException(type=ImageErrorException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    List<CodeInfo> detectAndGetCodeInfoMat(
        @ThriftField(value=1, name="matType", requiredness=Requiredness.OPTIONAL) final MatType matType,
        @ThriftField(value=2, name="matData", requiredness=Requiredness.OPTIONAL) final byte [] matData,
        @ThriftField(value=3, name="width", requiredness=Requiredness.REQUIRED) final int width,
        @ThriftField(value=4, name="height", requiredness=Requiredness.REQUIRED) final int height
    ) throws ImageErrorException, ServiceRuntimeException;

    @ThriftMethod(value = "detectAndSearchFaces",
                  exception = {
                      @ThriftException(type=ImageErrorException.class, id=1),
                      @ThriftException(type=NotFaceDetectedException.class, id=2),
                      @ThriftException(type=ServiceRuntimeException.class, id=3)
                  })
    List<SearchResult> detectAndSearchFaces(
        @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
        @ThriftField(value=2, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
        @ThriftField(value=3, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
        @ThriftField(value=4, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws ImageErrorException, NotFaceDetectedException, ServiceRuntimeException;

    @ThriftMethod(value = "getCodeInfo",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    CodeInfo getCodeInfo(
        @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getCodeInfoByFeatureId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    CodeInfo getCodeInfoByFeatureId(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getCodeInfoByImageMd5",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    CodeInfo getCodeInfoByImageMd5(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getCodeInfosByFeatureId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<CodeInfo> getCodeInfosByFeatureId(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getCodeInfosByImageMd5",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<CodeInfo> getCodeInfosByImageMd5(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFace",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    FaceBean getFace(
        @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFaceByFeatureId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    FaceBean getFaceByFeatureId(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFaceByImageMd5",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    FaceBean getFaceByImageMd5(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFaceCount",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    int getFaceCount(
        @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFacesByFeatureId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<FaceBean> getFacesByFeatureId(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFacesByImageMd5",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<FaceBean> getFacesByImageMd5(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFeature",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    FeatureBean getFeature(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFeatureByFaceId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    FeatureBean getFeatureByFaceId(
        @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFeatureByImageMd5",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    FeatureBean getFeatureByImageMd5(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getFeatureCount",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    int getFeatureCount() throws ServiceRuntimeException;

    @ThriftMethod(value = "getFeaturesByImageMd5",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<FeatureBean> getFeaturesByImageMd5(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImage",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    ImageBean getImage(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImageByFaceId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    ImageBean getImageByFaceId(
        @ThriftField(value=1, name="faceId", requiredness=Requiredness.REQUIRED) final int faceId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImageByFeatureId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    ImageBean getImageByFeatureId(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImageBytes",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    byte [] getImageBytes(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImageBytesRef",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    byte [] getImageBytesRef(
        @ThriftField(value=1, name="primaryKey", requiredness=Requiredness.OPTIONAL) final String primaryKey,
        @ThriftField(value=2, name="refType", requiredness=Requiredness.OPTIONAL) final String refType
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImageCount",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    int getImageCount(
        @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImageRef",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    ImageBean getImageRef(
        @ThriftField(value=1, name="primaryKey", requiredness=Requiredness.OPTIONAL) final String primaryKey,
        @ThriftField(value=2, name="refType", requiredness=Requiredness.OPTIONAL) final String refType
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "getImagesByFeatureId",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<ImageBean> getImagesByFeatureId(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "hasFeature",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    boolean hasFeature(
        @ThriftField(value=1, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "hasFeatureByMD5",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    boolean hasFeatureByMD5(
        @ThriftField(value=1, name="featureId", requiredness=Requiredness.OPTIONAL) final String featureId
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "hasImage",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    boolean hasImage(
        @ThriftField(value=1, name="imageMd5", requiredness=Requiredness.OPTIONAL) final String imageMd5
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "isLocal",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    boolean isLocal() throws ServiceRuntimeException;

    @ThriftMethod(value = "loadFeaturesMd5ByCreateTime",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<String> loadFeaturesMd5ByCreateTime(
        @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final Long timestamp
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "loadFeaturesMd5ByCreateTimeTimeStr",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<String> loadFeaturesMd5ByCreateTimeTimeStr(
        @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final String timestamp
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "loadFeaturesMd5ByWhere",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<String> loadFeaturesMd5ByWhere(
        @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "loadImagesByWhere",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<ImageBean> loadImagesByWhere(
        @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where,
        @ThriftField(value=2, name="startRow", requiredness=Requiredness.REQUIRED) final int startRow,
        @ThriftField(value=3, name="numRows", requiredness=Requiredness.REQUIRED) final int numRows
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "loadImagesMd5ByCreateTime",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<String> loadImagesMd5ByCreateTime(
        @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final Long timestamp
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "loadImagesMd5ByCreateTimeTimeStr",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<String> loadImagesMd5ByCreateTimeTimeStr(
        @ThriftField(value=1, name="timestamp", requiredness=Requiredness.OPTIONAL) final String timestamp
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "loadImagesMd5ByWhere",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<String> loadImagesMd5ByWhere(
        @ThriftField(value=1, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws ServiceRuntimeException;

    @ThriftMethod(value = "searchFaces",
                  exception = {
                      @ThriftException(type=ImageErrorException.class, id=1),
                      @ThriftException(type=NotFaceDetectedException.class, id=2),
                      @ThriftException(type=ServiceRuntimeException.class, id=3)
                  })
    List<SearchResult> searchFaces(
        @ThriftField(value=1, name="imgData", requiredness=Requiredness.OPTIONAL) final byte [] imgData,
        @ThriftField(value=2, name="facePos", requiredness=Requiredness.OPTIONAL) final CodeInfo facePos,
        @ThriftField(value=3, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
        @ThriftField(value=4, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
        @ThriftField(value=5, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws ImageErrorException, NotFaceDetectedException, ServiceRuntimeException;

    @ThriftMethod(value = "searchFacesMat",
                  exception = {
                      @ThriftException(type=NotFaceDetectedException.class, id=1),
                      @ThriftException(type=ServiceRuntimeException.class, id=2)
                  })
    List<SearchResult> searchFacesMat(
        @ThriftField(value=1, name="matType", requiredness=Requiredness.OPTIONAL) final MatType matType,
        @ThriftField(value=2, name="matData", requiredness=Requiredness.OPTIONAL) final byte [] matData,
        @ThriftField(value=3, name="width", requiredness=Requiredness.REQUIRED) final int width,
        @ThriftField(value=4, name="height", requiredness=Requiredness.REQUIRED) final int height,
        @ThriftField(value=5, name="facePos", requiredness=Requiredness.OPTIONAL) final CodeInfo facePos,
        @ThriftField(value=6, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
        @ThriftField(value=7, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
        @ThriftField(value=8, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws NotFaceDetectedException, ServiceRuntimeException;

    @ThriftMethod(value = "searchFeatures",
                  exception = {
                      @ThriftException(type=ServiceRuntimeException.class, id=1)
                  })
    List<SearchResult> searchFeatures(
        @ThriftField(value=1, name="feature", requiredness=Requiredness.OPTIONAL) final byte [] feature,
        @ThriftField(value=2, name="similarty", requiredness=Requiredness.REQUIRED) final double similarty,
        @ThriftField(value=3, name="rows", requiredness=Requiredness.REQUIRED) final int rows,
        @ThriftField(value=4, name="where", requiredness=Requiredness.OPTIONAL) final String where
    ) throws ServiceRuntimeException;
}