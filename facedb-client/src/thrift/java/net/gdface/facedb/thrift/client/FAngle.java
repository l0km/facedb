package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("FAngle")
public final class FAngle
{
    public FAngle() {
    }

    private double confidence;

    @ThriftField(value=1, name="confidence", requiredness=Requiredness.REQUIRED)
    public double getConfidence() { return confidence; }

    @ThriftField
    public void setConfidence(final double confidence) { this.confidence = confidence; }

    private int pitch;

    @ThriftField(value=2, name="pitch", requiredness=Requiredness.REQUIRED)
    public int getPitch() { return pitch; }

    @ThriftField
    public void setPitch(final int pitch) { this.pitch = pitch; }

    private int roll;

    @ThriftField(value=3, name="roll", requiredness=Requiredness.REQUIRED)
    public int getRoll() { return roll; }

    @ThriftField
    public void setRoll(final int roll) { this.roll = roll; }

    private int yaw;

    @ThriftField(value=4, name="yaw", requiredness=Requiredness.REQUIRED)
    public int getYaw() { return yaw; }

    @ThriftField
    public void setYaw(final int yaw) { this.yaw = yaw; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("confidence", confidence)
            .add("pitch", pitch)
            .add("roll", roll)
            .add("yaw", yaw)
            .toString();
    }
}
