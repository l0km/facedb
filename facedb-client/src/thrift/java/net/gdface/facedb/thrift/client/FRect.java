package net.gdface.facedb.thrift.client;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("FRect")
public final class FRect
{
    public FRect() {
    }

    private int height;

    @ThriftField(value=1, name="height", requiredness=Requiredness.REQUIRED)
    public int getHeight() { return height; }

    @ThriftField
    public void setHeight(final int height) { this.height = height; }

    private int left;

    @ThriftField(value=2, name="left", requiredness=Requiredness.REQUIRED)
    public int getLeft() { return left; }

    @ThriftField
    public void setLeft(final int left) { this.left = left; }

    private int top;

    @ThriftField(value=3, name="top", requiredness=Requiredness.REQUIRED)
    public int getTop() { return top; }

    @ThriftField
    public void setTop(final int top) { this.top = top; }

    private int width;

    @ThriftField(value=4, name="width", requiredness=Requiredness.REQUIRED)
    public int getWidth() { return width; }

    @ThriftField
    public void setWidth(final int width) { this.width = width; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("height", height)
            .add("left", left)
            .add("top", top)
            .add("width", width)
            .toString();
    }
}
