package net.gdface.facedb.client;

import static org.junit.Assert.*;

import org.junit.Test;

import com.gitee.l0km.xthrift.thrift.TypeTransformer;

import net.gdface.sdk.CodeInfo;

public class TypeTransformerTest {

	@Test
	public void test() {
		try{
			CodeInfo src = new CodeInfo();
			net.gdface.facedb.thrift.client.CodeInfo dst = TypeTransformer.getInstance().to(src, CodeInfo.class, net.gdface.facedb.thrift.client.CodeInfo.class);
			System.out.println(dst.toString());
			net.gdface.facedb.thrift.client.CodeInfo dst1 = new net.gdface.facedb.thrift.client.CodeInfo();
			CodeInfo dst2 = TypeTransformer.getInstance().to(dst1, net.gdface.facedb.thrift.client.CodeInfo.class, CodeInfo.class);
			System.out.println(dst2.toString());
			//		net.gdface.facedb.thrift.client.CodeInfo dst3 = TypeTransformer.getInstance().to(dst2, CodeInfo.class, net.gdface.facedb.thrift.client.CodeInfo.class);
			//		System.out.println(dst3.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Test
	public void test2(){
		net.gdface.facedb.thrift.client.CodeInfo src = new net.gdface.facedb.thrift.client.CodeInfo();
		net.gdface.sdk.decorator.client.CodeInfo dst = TypeTransformer.getInstance().to(src, net.gdface.facedb.thrift.client.CodeInfo.class, net.gdface.sdk.decorator.client.CodeInfo.class);
		System.out.println(dst.toString());
		net.gdface.facedb.thrift.client.CodeInfo dst2 = TypeTransformer.getInstance().to(dst, net.gdface.sdk.decorator.client.CodeInfo.class, net.gdface.facedb.thrift.client.CodeInfo.class);
		System.out.println(dst2.toString());
		CodeInfo dst3 = TypeTransformer.getInstance().to(dst, net.gdface.sdk.decorator.client.CodeInfo.class, CodeInfo.class);
		System.out.println(dst3.toString());
		net.gdface.sdk.decorator.client.CodeInfo dst4 = TypeTransformer.getInstance().to(dst3, CodeInfo.class, net.gdface.sdk.decorator.client.CodeInfo.class);
		System.out.println(dst4.toString());
		net.gdface.facedb.thrift.client.CodeInfo dst5 = TypeTransformer.getInstance().to(dst4, net.gdface.sdk.decorator.client.CodeInfo.class,net.gdface.facedb.thrift.client.CodeInfo.class);
		System.out.println(dst5.toString());
	}
}
