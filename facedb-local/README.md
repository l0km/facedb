# store_util

FaceDB中图像数据可以存储于数据库中(`fd_store`表)[存储类型`DB`],也可以以文件形式存储于本地文件夹(默认存储位置`${user.home}/.facedb/store`)[存储类型`FILE`],默认存储类型为`DB`.

可以通过在`${user.home}/.facedb/config.properties`设置`store.target`属性来改变存储类型,示例如下：
	
	# 指定存储类型为FILE
	store.target=FILE
	# 指定存储类型为FILE时,数据的存储位置
	store.local.root=d:\\image


store_util用于管理FaceDB存储的图像数据，可以在改变存储类型，执行store_util自动将图像转换为指定的存储类型，`store_util` 的使用说明如下:
	
	usage: StoreUtil [options]
	 -cf,--cascade        if true,remove features that associated with image
	                      when remove image row,default:false
	 -cp,--copy           if true, do not delete the old store data when store
	                      to new store target,default:false
	 -h,--help            Print this usage information
	 -ow,--overwrite      if true,force overwrite exist data(file)
	                      ,default:false
	 -ri,--remove         if true, remove image row if image data is
	                      absent,default:false
	 -st,--target <arg>   store target,available value: FILE[store as local
	                      file],DB(store in fl_store table),default : depend
	                      on value of property 'store.target' in
	                      config.property