package net.gdface.facedb;

import java.io.File;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Test;

import gu.sql2java.manager.SqliteInitializer;

public class SqliteTest {


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		URL createSql = SqliteTest.class.getResource("/sql/create_table.sql");
		File db = new File("facedb.db");
		SqliteInitializer.init(db, createSql, true, null);
	}
	@Test
	public void testInit() {
	}

}
