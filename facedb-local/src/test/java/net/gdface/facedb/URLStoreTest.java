package net.gdface.facedb;

import java.net.URL;
import java.net.URLConnection;

import org.junit.BeforeClass;
import org.junit.Test;

import gu.sql2java.manager.store.BinaryStoreTable;
import gu.sql2java.store.LocalBinaryStore;

import static gu.sql2java.SimpleLog.*;
public class URLStoreTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() throws Exception {
		{
			new BinaryStoreTable("fd_store","data", null, null).intall();

			URL storeURL = new URL("fd-store:///data/efc591808992c9c7a0478550415d93b4");
			log("store url host ={}",storeURL.getHost());
			log("store url port ={}",storeURL.getPort());
			log("store url path ={}",storeURL.getPath());
		}
		LocalBinaryStore.SINGLETON.intall();
		{
			URL localURL = new URL("lbs:///efc591808992c9c7a0478550415d93b4");
			log("local url host ={}",localURL.getHost());
			log("local url port ={}",localURL.getPort());
			log("local url path ={}",localURL.getPath());
		}
		
	}
	@Test
	public void testGetFileNameMap(){
		String mimeType = URLConnection.guessContentTypeFromName(".PNG");
	    System.out.printf("png mimeType %s\n", mimeType);
	    System.out.printf("jpg mimeType %s\n", URLConnection.guessContentTypeFromName(".jpg"));
	    System.out.printf("jpeg mimeType %s\n", URLConnection.guessContentTypeFromName(".jpeg"));
	    System.out.printf("bmp mimeType %s\n", URLConnection.guessContentTypeFromName(".bmp"));

	}
	
}
