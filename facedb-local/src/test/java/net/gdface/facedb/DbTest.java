package net.gdface.facedb;

import org.junit.BeforeClass;
import org.junit.Test;

import gu.sql2java.TableManager;
import gu.sql2java.manager.Managers;
import net.gdface.facedb.db.IImageManager;
import net.gdface.facedb.db.ImageBean;
import net.gdface.facedb.db.StoreBean;

public class DbTest {
	private static IImageManager imgmgr;
	private static TableManager<ImageBean> imgmgr2;
	private static TableManager<StoreBean> storemgr2;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		imgmgr = Managers.instanceOf(IImageManager.class);
		imgmgr2 = Managers.managerOf(ImageBean.class);
		storemgr2 = Managers.managerOf(StoreBean.class);

	}

	@Test
	public void test1LoadByPrimaryKey() {
		String md5 = "9aad841d21c87c940663203b5e6bec0c";
		ImageBean bean = imgmgr.loadByPrimaryKey(md5);
		System.out.printf("image bean %s", bean);
	}
	@Test
	public void test2LoadByPrimaryKey() {
		String md5 = "00a4e7aaab31deb44746e48ab864fb75";
		ImageBean image = imgmgr2.loadByPrimaryKeyChecked(md5);
		System.out.printf("image bean %s\n", image);
		StoreBean store = storemgr2.loadByPrimaryKeyChecked(md5);
		System.out.printf("image bean %s\n", store);


	}
}
