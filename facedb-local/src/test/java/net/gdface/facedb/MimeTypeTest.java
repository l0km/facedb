package net.gdface.facedb;

import static org.junit.Assert.*;

import java.net.URLConnection;

import org.apache.tika.Tika;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.junit.BeforeClass;
import org.junit.Test;

public class MimeTypeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() throws MimeTypeException {
		MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
		{
			MimeType jpeg = allTypes.forName("image/jpeg");
			String jpegExt = jpeg.getExtension(); // .jpg
			System.out.printf("mime jpeg=%s\n", jpeg);
			System.out.printf("jpegExt=%s\n", jpegExt);
			
			assertEquals(".jpg", jpeg.getExtension());
		}
		{
			String mime = URLConnection.guessContentTypeFromName("1.jpeg");
			System.out.printf("mime jpeg=%s\n", mime);
		}
	}

}
