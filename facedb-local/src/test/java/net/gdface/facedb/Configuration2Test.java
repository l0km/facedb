package net.gdface.facedb;

import java.util.Properties;

import org.apache.commons.configuration2.interpol.ConfigurationInterpolator;
import org.apache.commons.configuration2.interpol.InterpolatorSpecification;
import org.junit.BeforeClass;
import org.junit.Test;

public class Configuration2Test implements LocalConstant {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		InterpolatorSpecification spec =
				new InterpolatorSpecification.Builder()
				.withPrefixLookups(ConfigurationInterpolator.getDefaultPrefixLookups())
				.withDefaultLookups(ConfigurationInterpolator.getDefaultPrefixLookups().values())
				.create();
		ConfigurationInterpolator interpolator = ConfigurationInterpolator.fromSpecification(spec);
		System.out.printf("${sys:user.home}=%s\n",interpolator.interpolate("${sys:user.home}/WWW"));

		System.out.printf("${env.REDIS_HOME}=%s\n",interpolator.interpolate("${env:REDIS_HOME}"));
		System.out.printf("${env.HOMEPATH}=%s\n",interpolator.interpolate("${env:HOMEPATH}"));

		System.out.printf("HOME=%s\n",System.getenv("HOME"));
	}
	@Test
	public void test2(){
		Properties props = PropertyUtil.loadPropertiesInUserHome(HOME_FOLDER + "/" + PROPFILE);	
		props.list(System.out);
		String v = props.getProperty(STORE_LOCAL_ROOT, DEFAULT_LBS_ROOT);
		System.out.printf("%s=%s\n", STORE_LOCAL_ROOT,v);
	}
}
