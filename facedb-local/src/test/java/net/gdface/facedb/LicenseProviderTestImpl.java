package net.gdface.facedb;

import net.facelib.mtfsdk.FaceApiMtf;
import net.gdface.license.GfLicenseProvider;
import net.gdface.license.RegisterException;

/**
 * 向SDK提供授权信息的 {@link GfLicenseProvider}实现.
 * 必须有无参构造函数
 * @author  Guyadong
 */
public class LicenseProviderTestImpl implements GfLicenseProvider {
    private String licenseCode;
    public LicenseProviderTestImpl(){
    }
    @Override
    public String getLicenseKey() {
        /* 返回SDK厂商提供的授权关键字 */
        return "your.license.key";
    }

    @Override
    public String getLicenseCode() {
        if(licenseCode == null){
            try {
                licenseCode = FaceApiMtf.licenseManager().licenseOnline(getLicenseKey());
            } catch (RegisterException e) {
                throw new RuntimeException(e);
            }
        }
        return licenseCode;
    }

    @Override
    public void saveLicenseCode(String s) {
        licenseCode = s;
    }
}
