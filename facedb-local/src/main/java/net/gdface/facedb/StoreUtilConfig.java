package net.gdface.facedb;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.gitee.l0km.com4j.cli.BaseAppConfig;
import com.google.common.base.Strings;

import net.gdface.facedb.CommonConstant;
import net.gdface.facedb.LocalConstant.StoreType;

/**
 * 命令行工具配置参数
 * @author guyadong
 *
 */
public class StoreUtilConfig extends BaseAppConfig implements CommonConstant,StoreUtilConstant {

	static final  StoreUtilConfig STORE_CONFIG = new StoreUtilConfig();
	private StoreType storeTarget;
	private boolean removeImageBeanIfAbsent;
	private boolean overwrite;
	private boolean cascadeFeature;
	private boolean copy;
	public StoreUtilConfig() {
	
		options.addOption(Option.builder(STORE_TARGE_OPTION).longOpt(STORE_TARGE_OPTION_LONG)
				.desc(STORE_TARGE_OPTION_DESC).numberOfArgs(1).build());
		options.addOption(Option.builder(REMOVE_IMAGE_IFABSENT_OPTION).longOpt(REMOVE_IMAGE_IFABSENT_OPTION_LONG)
				.desc(REMOVE_IMAGE_IFABSENT_OPTION_DESC).numberOfArgs(0).build());
		options.addOption(Option.builder(OVERWRITE_OPTION).longOpt(OVERWRITE_OPTION_LONG)
				.desc(OVERWRITE_OPTION_DESC).numberOfArgs(0).build());
		options.addOption(Option.builder(CASCADE_FEATURE_OPTION).longOpt(CASCADE_FEATURE_OPTION_LONG)
				.desc(CASCADE_FEATURE_OPTION_DESC).numberOfArgs(0).build());
		options.addOption(Option.builder(ISCOPY_OPTION).longOpt(ISCOPY_OPTION_LONG)
				.desc(ISCOPY_OPTION_DESC).numberOfArgs(0).build());

		defaultValue.setProperty(STORE_TARGE_OPTION_LONG,"");

	}

	@Override
	public void loadConfig(Options options, CommandLine cmd) throws ParseException {
		super.loadConfig(options, cmd);
		this.removeImageBeanIfAbsent = (Boolean)getProperty(REMOVE_IMAGE_IFABSENT_OPTION_LONG);
		this.overwrite = (Boolean)getProperty(OVERWRITE_OPTION_LONG);
		this.cascadeFeature = (Boolean)getProperty(CASCADE_FEATURE_OPTION_LONG);
		this.copy = (Boolean)getProperty(ISCOPY_OPTION_LONG);
		String v = (String)getProperty(STORE_TARGE_OPTION_LONG);
		if(!Strings.isNullOrEmpty(v)){
			try {
				this.storeTarget = StoreType.valueOf(v.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new ParseException(String.format("INVALID option %s = %s", STORE_TARGE_OPTION_LONG, v));
			}
		}
	}
	@Override
	protected String getAppName() {
		return StoreUtil.class.getSimpleName();
	}

	public StoreType getStoreTarget() {
		return storeTarget;
	}

	public boolean isRemoveImageBeanIfAbsent() {
		return removeImageBeanIfAbsent;
	}

	public boolean isOverwrite() {
		return overwrite;
	}

	public boolean isCascadeFeature() {
		return cascadeFeature;
	}

	public boolean isCopy() {
		return copy;
	}
	
}
