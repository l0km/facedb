package net.gdface.facedb;

public interface StoreUtilConstant {
	public static final String STORE_TARGE_OPTION = "st";
	public static final String STORE_TARGE_OPTION_LONG = "target";
	public static final String STORE_TARGE_OPTION_DESC = "store target,available value: FILE[store as local file],DB(store in fl_store table),default : depend on value of property 'store.target' in config.property";
	public static final String REMOVE_IMAGE_IFABSENT_OPTION = "ri";
	public static final String REMOVE_IMAGE_IFABSENT_OPTION_LONG = "remove";
	public static final String REMOVE_IMAGE_IFABSENT_OPTION_DESC = "if true, remove image row if image data is absent,default:false";
	public static final String OVERWRITE_OPTION = "ow";
	public static final String OVERWRITE_OPTION_LONG = "overwrite";
	public static final String OVERWRITE_OPTION_DESC = "if true,force overwrite exist data(file) ,default:false";
	public static final String CASCADE_FEATURE_OPTION = "cf";
	public static final String CASCADE_FEATURE_OPTION_LONG = "cascade";
	public static final String CASCADE_FEATURE_OPTION_DESC = "if true,remove features that associated with image when remove image row,default:false";
	public static final String ISCOPY_OPTION = "cp";
	public static final String ISCOPY_OPTION_LONG = "copy";
	public static final String ISCOPY_OPTION_DESC = "if true, do not delete the old store data when store to new store target,default:false";

}
