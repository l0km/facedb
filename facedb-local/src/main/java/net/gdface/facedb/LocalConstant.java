package net.gdface.facedb;

/**
 * 服务端常量定义
 * @author guyadong
 *
 */
public interface LocalConstant  extends CommonConstant{
	/**
	 * 配置文件名
	 */
	public static final String PROPFILE="config.properties";
	/**
	 * HOME文件夹下的配置文件夹名
	 */
	public static final String HOME_FOLDER = ".facedb";
	/**
	 * 默认本地文件存储根文件夹
	 */
	public static final String DEFAULT_LBS_ROOT = "${sys:user.home}/" + HOME_FOLDER + "/store";
	/**
	 * 默认二进制数据存储类型
	 */
	public static final String DEFAULT_STORE_TARGET = StoreType.DB.name();
	
	/** 数据库配置: 缓存有效期(分钟),默认:10分钟 */
	public static final String DB_CACHE_DURATION = "db.cache.duration";
	
	/** 数据库配置: 缓存最大容量,默认:10000条记录 */
	public static final String DB_CACHE_MAXIMUMSIZE = "db.cache.maximumSize";
	
	/** URL本地存储配置: 存储根路径 */
	public static final String STORE_LOCAL_ROOT = "store.local.root";
	
	/** 存储类型: FILE:本地文件存储,DB:数据库存储  */
	public static final String STORE_TARGET = "store.target";
    
    /** 系统日志配置:是否输出调试信息,默认:false */
    public static final String SYSLOG_COREDEBUG = "syslog.coreDebug";
    
    /** SQLite数据库配置:是否以内存方式运行,默认:false */
    public static final String SQLITE_RUNINMEMORY = "sqlite.runInMemory";
    
    /** SQLite数据库配置:备份时间间隔(秒),以内存方式运行时有效,默认:300秒*/
    public static final String SQLITE_BACKUP_INTERNAL = "sqlite.backupinternal";
    
    /** SQLite数据库配置:数据文件位置 */
    public static final String SQLITE_DB = "sqlite.db";
	/**
	 * 二进制文件存储方式
	 */
	public enum StoreType{
		/** 数据库存储 (fd_store表) */DB,
		/** 本地文件存储 */FILE
		}
}
