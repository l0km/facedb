package net.gdface.facedb;

import java.io.File;
import java.util.Properties;

public class TableManagerInitializerSupport {
	static File localPropertiesFile = null;
	static Properties properties;

	private TableManagerInitializerSupport() {
	}
	/**
	 * 设置本地配置参数文件位置,默认为{@code null}
	 * @param localPropertiesFile
	 */
	public static  void setLocalPropertiesFile(File localPropertiesFile) {
		TableManagerInitializerSupport.localPropertiesFile = localPropertiesFile;
	}
	/**
	 * 设置本地配置参数文件位置,默认为{@code null}
	 * @param localPropertiesFile
	 */
	public static void setLocalPropertiesFile(Properties properties) {
		TableManagerInitializerSupport.properties = properties;
	}
}
