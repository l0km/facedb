package net.gdface.facedb;

import com.gitee.l0km.com4j.base.BinaryUtils;

import net.gdface.facedb.db.FeatureBean;
import net.gdface.sdk.fse.BaseFseDbEngine;
import net.gdface.sdk.fse.FeatureSe;
/**
 * 基于数据库的特征搜索引擎
 * @author guyadong
 *
 */
public class FeatureDbSearchEngine extends BaseFseDbEngine<FeatureBean> {
	/**
	 * 
	 * @param fse {@link FeatureSe}引擎实例
	 */
	public FeatureDbSearchEngine(FeatureSe fse) {
		super(fse);
	}

	@Override
	protected byte[] featureOf(FeatureBean bean) {
		return null == bean ? null : BinaryUtils.getBytesInBuffer(bean.getFeature());
	}

	@Override
	protected byte[] featureIdOf(FeatureBean bean) {
		return null == bean ? null : BinaryUtils.hex2Bytes(bean.getMd5());
	}

}
