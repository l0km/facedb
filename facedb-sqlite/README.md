# facedb-sqlite

提供SQLite本地数据库的`net.gdface.facedb.FaceDb`实例.

参见 [`net.gdface.facedb.FacedbSqliteInitializer`](src/main/java/net/gdface/facedb/FacedbSqliteInitializer.java)

`FacedbSqliteInitializer`调用方法参见测试代码:

[net.gdface.facedb.FacedbSqliteTest](src/test/java/net/gdface/facedb/FacedbSqliteTest.java)

## android 平台下使用说明

### libsqlitejdbc.so
facedb-sqlite是基于`org.xerial:sqlite-jdbc:3.32.3.2`提供的JDBC驱动实现应用层对sqlite数据库透明操作。
  
`sqlite-jdbc-3.32.3.2.jar`中提供了linux,windows,mac,android等所有主流操作系统及硬件平台(x86,x86_64,arm...)的sqlitejdbc动态库.(参见 jar 包中 `org/sqlite/native`文件夹).  
  
android-arm的动态库放在 `org/sqlite/native/linux/android-arm`下。我们知道这个路径并不是android所能识别的标准的动态库存放位置，所以如果直接引用`org.xerial:sqlite-jdbc`库,在程序运行调用SQLite JDBC接口时会报错提示找不到`libsqlitejdbc.so`.  
    
为解决这个问题需要应用层开发者手工从`sqlite-jdbc-3.32.3.2.jar`中将`org/sqlite/native/linux/android-arm/libsqlitejdbc.so`解压出来，保存到自己的APP项目路径下：`app/src/main/jniLibs/armeabi-v7a/libsqlitejdbc.so`  

### com.gitee.l0km:common-image

facedb-sqlite默认依赖了`com.gitee.l0km:common-image`,而这个库并不支持android平台,所以直接引用的话运行时会报错.

为解决这个问题,需要排除掉`com.gitee.l0km:common-image`用`com.gitee.l0km:common-image-android`代替,所以需要修改app.gradle文件


    implementation ('com.gitee.l0km:facedb-sqlite:2.4.1-SNAPSHOT'){
        exclude  group :'com.gitee.l0km', module: 'common-image' // 排除com.gitee.l0km:common-image
    }
	implementation (com.gitee.l0km:common-image-android:1.4.5) // 增加com.gitee.l0km:common-image-android
