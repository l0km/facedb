// ______________________________________________________
// Generated by sql2java - https://github.com/10km/sql2java 
// JDBC driver used at code generation time: com.mysql.jdbc.Driver
// template: manager.interface.java.vm
// ______________________________________________________
package net.gdface.facedb.db;
import gu.sql2java.TableManager;
import gu.sql2java.exception.ObjectRetrievalException;
import gu.sql2java.exception.RuntimeDaoException;

/**
 * Interface to handle database calls (save, load, count, etc...) for the fd_feature table.<br>
 * Remarks: 用于验证身份的人脸特征数据表
 * @author guyadong
 */
public interface IFeatureManager extends TableManager<FeatureBean>
{  
    //////////////////////////////////////
    // PRIMARY KEY METHODS
    //////////////////////////////////////

    //1
    /**
     * Loads a {@link FeatureBean} from the fd_feature using primary key fields.
     *
     * @param md5 String - PK# 1
     * @return a unique FeatureBean or {@code null} if not found
     * @throws RuntimeDaoException
     */
    public FeatureBean loadByPrimaryKey(String md5)throws RuntimeDaoException;

    //1.1
    /**
     * Loads a {@link FeatureBean} from the fd_feature using primary key fields.
     *
     * @param md5 String - PK# 1
     * @return a unique FeatureBean
     * @throws ObjectRetrievalException if not found
     * @throws RuntimeDaoException
     */
    public FeatureBean loadByPrimaryKeyChecked(String md5) throws RuntimeDaoException,ObjectRetrievalException;
    
    //1.4
    /**
     * check if contains row with primary key fields.
     * @param md5 String - PK# 1
     * @return true if this fd_feature contains row with primary key fields.
     * @throws RuntimeDaoException
     */
    public boolean existsPrimaryKey(String md5)throws RuntimeDaoException;
    //1.4.1
    /**
     * Check duplicated row by primary keys,if row exists throw exception
     * @param md5 primary keys
     * @return md5
     * @throws RuntimeDaoException
     * @throws ObjectRetrievalException
     */
    public String checkDuplicate(String md5)throws RuntimeDaoException,ObjectRetrievalException;
    //1.8
    /**
     * Loads {@link FeatureBean} from the fd_feature using primary key fields.
     *
     * @param keys primary keys array
     * @return list of FeatureBean
     * @throws RuntimeDaoException
     */
    public java.util.List<FeatureBean> loadByPrimaryKey(String... keys)throws RuntimeDaoException;
    //1.9
    /**
     * Loads {@link FeatureBean} from the fd_feature using primary key fields.
     *
     * @param keys primary keys collection
     * @return list of FeatureBean
     * @throws RuntimeDaoException
     */
    public java.util.List<FeatureBean> loadByPrimaryKey(java.util.Collection<String> keys)throws RuntimeDaoException;
    //2
    /**
     * Delete row according to its primary keys.<br>
     * all keys must not be null
     *
     * @param md5 String - PK# 1
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     */
    public int deleteByPrimaryKey(String md5)throws RuntimeDaoException;
    //2.2
    /**
     * Delete rows according to primary key.<br>
     *
     * @param keys primary keys array
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     * @see #delete(gu.sql2java.BaseBean)
     */
    public int deleteByPrimaryKey(String... keys)throws RuntimeDaoException;
    //2.3
    /**
     * Delete rows according to primary key.<br>
     *
     * @param keys primary keys collection
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     * @see #delete(gu.sql2java.BaseBean)
     */
    public int deleteByPrimaryKey(java.util.Collection<String> keys)throws RuntimeDaoException;
 

    //////////////////////////////////////
    // GET/SET IMPORTED KEY BEAN METHOD
    //////////////////////////////////////
    //3.1 GET IMPORTED
    /**
     * Retrieves the {@link FaceBean} object from the fd_face.feature_md5 field.<BR>
     * FK_NAME : fd_face_ibfk_2 
     * @param bean the {@link FeatureBean}
     * @return the associated {@link FaceBean} beans or {@code null} if {@code bean} is {@code null}
     * @throws RuntimeDaoException
     */
    public FaceBean[] getFaceBeansByFeatureMd5(FeatureBean bean)throws RuntimeDaoException;
    
    //3.1.2 GET IMPORTED
    /**
     * Retrieves the {@link FaceBean} object from the fd_face.feature_md5 field.<BR>
     * FK_NAME : fd_face_ibfk_2 
     * @param md5OfFeature String - PK# 1
     * @return the associated {@link FaceBean} beans or {@code null} if {@code bean} is {@code null}
     * @throws RuntimeDaoException
     */
    public FaceBean[] getFaceBeansByFeatureMd5(String md5OfFeature)throws RuntimeDaoException;
    
    //3.2 GET IMPORTED
    /**
     * see also #getFaceBeansByFeatureMd5AsList(FeatureBean,int,int)
     * @param bean
     * @return import bean list
     * @throws RuntimeDaoException
     */
    public java.util.List<FaceBean> getFaceBeansByFeatureMd5AsList(FeatureBean bean)throws RuntimeDaoException;

    //3.2.2 GET IMPORTED
    /**
     * Retrieves the {@link FaceBean} object from fd_face.feature_md5 field.<BR>
     * FK_NAME:fd_face_ibfk_2
     * @param md5OfFeature String - PK# 1
     * @return the associated {@link FaceBean} beans 
     * @throws RuntimeDaoException
     */
    public java.util.List<FaceBean> getFaceBeansByFeatureMd5AsList(String md5OfFeature)throws RuntimeDaoException;
    //3.2.3 DELETE IMPORTED
    /**
     * delete the associated {@link FaceBean} objects from fd_face.feature_md5 field.<BR>
     * FK_NAME:fd_face_ibfk_2
     * @param md5OfFeature String - PK# 1
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     */
    public int deleteFaceBeansByFeatureMd5(String md5OfFeature)throws RuntimeDaoException;
    //3.2.4 GET IMPORTED
    /**
     * Retrieves the {@link FaceBean} object from fd_face.feature_md5 field.<BR>
     * FK_NAME:fd_face_ibfk_2
     * @param bean the {@link FeatureBean}
     * @param startRow the start row to be used (first row = 1, last row=-1)
     * @param numRows the number of rows to be retrieved (all rows = a negative number)
     * @return the associated {@link FaceBean} beans or empty list if {@code bean} is {@code null}
     * @throws RuntimeDaoException
     */
    public java.util.List<FaceBean> getFaceBeansByFeatureMd5AsList(FeatureBean bean,int startRow,int numRows)throws RuntimeDaoException;    
    //3.3 SET IMPORTED
    /**
     * set  the {@link FaceBean} object array associate to FeatureBean by the fd_face.feature_md5 field.<BR>
     * FK_NAME : fd_face_ibfk_2 
     * @param bean the referenced {@link FeatureBean}
     * @param importedBeans imported beans from fd_face
     * @return importedBeans always
     * @see IFaceManager#setReferencedByFeatureMd5(FaceBean, FeatureBean)
     * @throws RuntimeDaoException
     */
    public FaceBean[] setFaceBeansByFeatureMd5(FeatureBean bean , FaceBean[] importedBeans)throws RuntimeDaoException;

    //3.4 SET IMPORTED
    /**
     * set  the {@link FaceBean} object java.util.Collection associate to FeatureBean by the fd_face.feature_md5 field.<BR>
     * FK_NAME:fd_face_ibfk_2
     * @param bean the referenced {@link FeatureBean} 
     * @param importedBeans imported beans from fd_face 
     * @return importedBeans always
     * @see IFaceManager#setReferencedByFeatureMd5(FaceBean, FeatureBean)
     * @throws RuntimeDaoException
     */
    public <C extends java.util.Collection<FaceBean>> C setFaceBeansByFeatureMd5(FeatureBean bean , C importedBeans)throws RuntimeDaoException;

    //3.5 SYNC SAVE 
    /**
     * Save the FeatureBean bean and referenced beans and imported beans into the database.
     *
     * @param bean the {@link FeatureBean} bean to be saved
         * @param impFaceByFeatureMd5 the {@link FaceBean} bean refer to {@link FeatureBean} 
     * @return the inserted or updated {@link FeatureBean} bean
     * @throws RuntimeDaoException
     */
    public FeatureBean save(FeatureBean bean
        
        , FaceBean[] impFaceByFeatureMd5 )throws RuntimeDaoException;
    //3.6 SYNC SAVE AS TRANSACTION
    /**
     * Transaction version for sync save<br>
     * see also {@link #save(FeatureBean , FaceBean[] )}
     * @param bean the {@link FeatureBean} bean to be saved
         * @param impFaceByFeatureMd5 the {@link FaceBean} bean refer to {@link FeatureBean} 
     * @return the inserted or updated {@link FeatureBean} bean
     * @throws RuntimeDaoException
     */
    public FeatureBean saveAsTransaction(final FeatureBean bean
        
        ,final FaceBean[] impFaceByFeatureMd5 )throws RuntimeDaoException;
    //3.7 SYNC SAVE 
    /**
     * Save the FeatureBean bean and referenced beans and imported beans into the database.
     *
     * @param bean the {@link FeatureBean} bean to be saved
         * @param impFaceByFeatureMd5 the {@link FaceBean} bean refer to {@link FeatureBean} 
     * @return the inserted or updated {@link FeatureBean} bean
     * @throws RuntimeDaoException
     */
    public FeatureBean save(FeatureBean bean
        
        , java.util.Collection<FaceBean> impFaceByFeatureMd5 )throws RuntimeDaoException;
    //3.8 SYNC SAVE AS TRANSACTION
    /**
     * Transaction version for sync save<br>
     * see also {@link #save(FeatureBean , java.util.Collection )}
     * @param bean the {@link FeatureBean} bean to be saved
         * @param impFaceByFeatureMd5 the {@link FaceBean} bean refer to {@link FeatureBean} 
     * @return the inserted or updated {@link FeatureBean} bean
     * @throws RuntimeDaoException
     */
    public FeatureBean saveAsTransaction(final FeatureBean bean
        
        ,final  java.util.Collection<FaceBean> impFaceByFeatureMd5 )throws RuntimeDaoException;
  
    //45
    /**
     * return a primary key list from {@link FeatureBean} array
     * @param beans
     * @return primary key list
     */
    public java.util.List<String> toPrimaryKeyList(FeatureBean... beans);
    //46
    /**
     * return a primary key list from {@link FeatureBean} collection
     * @param beans
     * @return primary key list
     */
    public java.util.List<String> toPrimaryKeyList(java.util.Collection<FeatureBean> beans);

}
