set character set utf8;
SET NAMES 'utf8';
############################
# delete all table/view  ###
############################
DROP TABLE IF EXISTS fd_face ;
DROP TABLE IF EXISTS fd_feature ;
DROP TABLE IF EXISTS fd_image ;
DROP TABLE IF EXISTS fd_store ;

############################
# create all table/view  ###
############################
# 所有表中
# create_time 记录创建时间戳 (默认提供数据库服务器时间)
# update_time 记录创建时间戳 (默认提供数据库服务器时间)

CREATE TABLE IF NOT EXISTS fd_store (
  `md5`       char(32) NOT NULL PRIMARY KEY COMMENT '主键,md5检验码',
  `extension` varchar(32) DEFAULT NULL COMMENT '数据类型文件扩展名,such as jpeg',
  `data`      mediumblob COMMENT '二进制数据'
) COMMENT '二进制数据存储表' DEFAULT CHARSET=utf8;

/*
 删除 fd_image 中记录时会同步级联删除 fd_face 中 image_md5 关联的所有记录
*/
CREATE TABLE IF NOT EXISTS fd_image (
  `md5`         char(32) NOT NULL PRIMARY KEY COMMENT '主键,图像md5检验码,同时也是从 fd_store 获取图像数据的key',
  `format`      varchar(32)  COMMENT '图像格式,jpeg,png...', 
  `width`       int NOT NULL COMMENT '图像宽度',
  `height`      int NOT NULL COMMENT '图像高度',
  `depth`       int DEFAULT 0 NOT NULL  COMMENT '通道数',
  `face_num`    int DEFAULT 0 NOT NULL  COMMENT '图像中的人脸数目',
  `location`    varchar(64) DEFAULT NULL COMMENT '图像存储位置URL', 
  `update_time` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp DEFAULT CURRENT_TIMESTAMP
) COMMENT '图像信息存储表,用于存储系统中所有用到的图像数据,表中只包含图像基本信息' DEFAULT CHARSET=utf8;

/* 
 允许多个fd_face记录对应一个 fd_feature记录,以适应红外算法的特殊需要,同时满足服务器端负责对比计算的要求
*/
CREATE TABLE IF NOT EXISTS fd_feature (
  `md5`         char(32) NOT NULL PRIMARY KEY COMMENT '主键,特征码md5校验码',
  `feature`     blob     NOT NULL COMMENT '二进制特征数据',
  `create_time` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) COMMENT '用于验证身份的人脸特征数据表' DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS fd_face (
  `id`          integer  NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '主键',
  `image_md5`   char(32) NOT NULL COMMENT '外键,所属图像id',
  `feature_md5` char(32) DEFAULT NULL COMMENT '外键,人脸特征数据MD5 id',
  ###### 人脸检测基本信息 <<
  # 人脸位置坐标
  `face_left`   int NOT NULL ,
  `face_top`    int NOT NULL ,
  `face_width`  int NOT NULL ,
  `face_height` int NOT NULL ,
  # 眼睛位置 
  `eye_leftx`   int ,
  `eye_lefty`   int ,
  `eye_rightx`  int ,
  `eye_righty`  int ,
  # 嘴巴位置 
  `mouth_x`     int ,
  `mouth_y`     int ,
  # 鼻子位置
  `nose_x`      int ,
  `nose_y`      int ,
  # 人脸角度 
  `angle_yaw`   int ,
  `angle_pitch` int ,
  `angle_roll`  int ,
  `angle_confidence` float,
  ###### 人脸检测基本信息 >> 
  `ext_info`    blob DEFAULT NULL COMMENT '扩展字段,保存人脸检测基本信息之外的其他数据,内容由SDK负责解析',
  `create_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (image_md5)       REFERENCES fd_image(md5)   ON DELETE CASCADE,
  FOREIGN KEY (feature_md5)     REFERENCES fd_feature(md5) ON DELETE SET NULL
) COMMENT '人脸检测信息数据表,用于保存检测到的人脸的所有信息(特征数据除外)' DEFAULT CHARSET=utf8;
